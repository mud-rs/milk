use std::{
    borrow::Cow,
    mem,
};

use mudrs_extension::text::{
    Line,
    Lines,
    Span,
    Spans,
    Style,
};
use textwrap::core::Fragment;
use tui::text::StyledGrapheme;
use unicode_segmentation::UnicodeSegmentation;

pub trait Wrap<'a> {
    type Wrapped: 'a;
    fn wrap(&'a self, cols: u16) -> Self::Wrapped;
}

#[derive(Debug, Clone, Copy, Default)]
pub struct SpanRange {
    start_span: usize,
    start_offset: usize,
    end_span: usize,
    end_offset: usize,
}

#[derive(Debug, Clone, Copy)]
pub struct SpansWord {
    pub range: SpanRange,
    word_len: usize,
    trailing_ws_len: usize,
}

#[derive(Debug)]
pub struct WordSpans<'r> {
    view: SpanRange,
    source: &'r [Span],
    current: usize,
}

impl<'r> ExactSizeIterator for WordSpans<'r> {
    fn len(&self) -> usize {
        self.view.num_spans() - self.current
    }
}

#[derive(Debug, Clone)]
pub struct CowSpan<'r> {
    pub content: Cow<'r, str>,
    pub style: Style,
}

impl<'r> CowSpan<'r> {
    pub fn tui_styled_graphemes(&self) -> impl Iterator<Item = StyledGrapheme> {
        let mut style = tui::style::Style::reset();
        style = style.patch(self.style.into());
        self.content
            .graphemes(true)
            .map(move |s| StyledGrapheme { symbol: s, style })
    }
}

impl<'r> From<&'r Span> for CowSpan<'r> {
    fn from(other: &'r Span) -> Self {
        CowSpan {
            content: Cow::Borrowed(other.content.as_str()),
            style: other.style,
        }
    }
}

impl From<Span> for CowSpan<'static> {
    fn from(other: Span) -> Self {
        CowSpan {
            content: Cow::Owned(other.content),
            style: other.style,
        }
    }
}

impl<'r> Iterator for WordSpans<'r> {
    type Item = CowSpan<'r>;
    fn next(&mut self) -> Option<Self::Item> {
        let current = self.current;
        let total_spans = self.view.num_spans();

        if current >= total_spans {
            return None;
        }
        self.current += 1;
        let is_first = current == 0;
        let is_last = current == total_spans - 1;

        let source_span = &self.source[self.view.start_span + current];

        let mut start_offset = 0usize;
        let mut end_offset = source_span.content.len();

        if is_first {
            start_offset = self.view.start_offset;
        }

        if is_last && self.view.end_offset != 0 {
            end_offset = self.view.end_offset;
        }

        let content: &str = &source_span.content[start_offset..end_offset];

        Some(CowSpan {
            content: Cow::Borrowed(content),
            style: source_span.style,
        })
    }
}

impl SpanRange {
    fn new_from(other: SpanRange) -> Self {
        SpanRange {
            start_span: other.end_span,
            start_offset: other.end_offset,
            end_span: other.end_span,
            end_offset: other.end_offset,
        }
    }

    fn num_spans(&self) -> usize {
        let spans = self.end_span - self.start_span;

        // If there's no offset into the end span, the final span will be the
        // span before the end span.
        // If there *is* an offset, we need to account for that by adding one to
        // the total number of spans.

        if self.end_offset == 0 {
            spans
        } else {
            spans + 1
        }
    }

    pub fn spans(self, source: &[Span]) -> WordSpans {
        WordSpans {
            source,
            view: self,
            current: 0,
        }
    }

    fn is_complete_word(&self, source: &[Span]) -> bool {
        let mut non_whitespace = false;
        let mut last_whitespace = false;

        for s in self.spans(source) {
            for c in s.content.chars() {
                non_whitespace = non_whitespace || !c.is_whitespace();
                last_whitespace = c.is_whitespace();
            }
        }

        non_whitespace && last_whitespace
    }

    fn inc_span(&mut self) {
        self.end_offset = 0;
        self.end_span += 1;
    }

    fn current_span<'r>(&self, source: &'r [Span]) -> Option<&'r Span> {
        source.get(self.end_span)
    }

    fn inc_offset(&mut self, by_char: char, source: &[Span]) {
        let current = if let Some(span) = self.current_span(source) {
            span
        } else {
            return;
        };
        let by = by_char.len_utf8();
        let new_off = self.end_offset + by;
        if current.content.len() == new_off {
            self.inc_span()
        } else {
            self.end_offset = new_off;
        }
    }

    fn to_word(self, source: &[Span]) -> SpansWord {
        let (_, word_len, trailing_ws_len) = self.spans(source).fold((false, 0, 0), |acc, s| {
            s.content
                .graphemes(Default::default())
                .fold(acc, |(mut f, mut w, mut t), g| {
                    let ws = g.chars().any(char::is_whitespace);
                    let l = unicode_width::UnicodeWidthStr::width(g);
                    f = f || !ws;
                    if f && ws {
                        t += l;
                    } else {
                        w += l;
                    }
                    (f, w, t)
                })
        });

        SpansWord {
            range: self,
            trailing_ws_len,
            word_len,
        }
    }
}

#[derive(Debug)]
struct WordsIter<'r> {
    current: SpanRange,
    spans: &'r [Span],
    finished: bool,
}

impl<'r> WordsIter<'r> {
    fn new(spans: &'r [Span]) -> Self {
        Self {
            spans,
            current: SpanRange::default(),
            finished: false,
        }
    }
}

impl<'r> Iterator for WordsIter<'r> {
    type Item = SpansWord;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            let current_span = if let Some(span) = self.spans.get(self.current.end_span) {
                // If the span is empty, skip it.
                if span.content.is_empty() {
                    self.current.inc_span();
                    continue;
                }
                span
            } else {
                if !self.finished {
                    self.finished = true;
                    return Some(self.current.to_word(self.spans));
                }
                return None;
            };
            let current_offset = self.current.end_offset;

            for c in current_span.content[current_offset..].chars() {
                if !c.is_whitespace() && self.current.is_complete_word(self.spans) {
                    let mut new = SpanRange::new_from(self.current);
                    new.inc_offset(c, self.spans);
                    let word = mem::replace(&mut self.current, new);
                    return Some(word.to_word(self.spans));
                }
                self.current.inc_offset(c, self.spans);
            }
        }
    }
}

impl Fragment for SpansWord {
    fn whitespace_width(&self) -> f64 {
        self.trailing_ws_len as f64
    }
    fn penalty_width(&self) -> f64 {
        0 as f64
    }
    fn width(&self) -> f64 {
        self.word_len as f64
    }
}

pub fn split_words(line: &[Span]) -> Vec<SpansWord> {
    WordsIter::new(line).collect()
}

pub fn wrap_line(line: &[SpansWord], width: usize) -> Vec<&[SpansWord]> {
    textwrap::wrap_algorithms::wrap_first_fit(line, &[width as f64])
}

pub struct CowLine<'a> {
    pub spans: Vec<CowSpan<'a>>,
}

pub struct CowSpans<'a> {
    pub spans: Vec<CowSpan<'a>>,
}

pub struct CowLines<'a> {
    pub full: Vec<CowLine<'a>>,
    pub r#final: Option<CowSpans<'a>>,
}

impl<'a> Wrap<'a> for Vec<Span> {
    type Wrapped = Lines;

    fn wrap(&'a self, cols: u16) -> Self::Wrapped {
        let words = split_words(self);
        let wrapped = wrap_line(&words, cols as usize);
        let wrapped_spans = wrapped
            .into_iter()
            .map(|l| l.iter().flat_map(|w| w.range.spans(self)));

        let mut out = Lines::default();
        for line in wrapped_spans {
            let mut new_line = vec![];
            for span in line {
                let content = span.content.into_owned();
                new_line.push(Span {
                    content,
                    style: span.style,
                });
            }
            if let Some(s) = new_line.last_mut() {
                s.content.truncate(s.content.trim_end().len());
            }
            out.push_line(Line(new_line));
        }

        if let Some(Line(l)) = out.full.pop() {
            out.r#final = Some(Spans(l));
        }

        out
    }
}

impl<'a> Wrap<'a> for Line {
    type Wrapped = Lines;
    fn wrap(&'a self, cols: u16) -> Self::Wrapped {
        let mut out = self.0.wrap(cols);
        out.complete_final();
        out
    }
}

impl<'a> Wrap<'a> for Spans {
    type Wrapped = Lines;
    fn wrap(&'a self, cols: u16) -> Self::Wrapped {
        self.0.wrap(cols)
    }
}

impl<'a> Wrap<'a> for Lines {
    type Wrapped = Lines;

    fn wrap(&'a self, cols: u16) -> Self::Wrapped {
        let mut out = Lines::default();
        for line in &self.full {
            let mut wrapped = line.wrap(cols);
            wrapped.complete_final();
            out.append(wrapped);
        }
        if let Some(r#final) = &self.r#final {
            out.append(r#final.wrap(cols));
        }
        out
    }
}
