use std::{
    collections::VecDeque,
    ops::RangeBounds,
};

use mudrs_extension::text::{
    Line,
    Lines,
    Span,
    Spans,
};

use crate::range::LinesRange;

#[derive(Clone, Debug)]
pub struct RingLines {
    pub full: VecDeque<Line>,
    pub r#final: Option<Spans>,
    capacity: usize,
}

impl Default for RingLines {
    fn default() -> Self {
        RingLines {
            full: Default::default(),
            r#final: Default::default(),
            capacity: 1000,
        }
    }
}

impl RingLines {
    fn drop_extra(&mut self) -> usize {
        let mut n = 0;
        while self.full.len() >= self.capacity {
            self.full.pop_front();
            n += 1;
        }
        n
    }
    fn push_line(&mut self, Line(mut line): Line) -> usize {
        let dropped = self.drop_extra();
        self.full
            .push_back(if let Some(Spans(mut completed)) = self.r#final.take() {
                completed.append(&mut line);
                Line(completed)
            } else {
                Line(line)
            });
        dropped
    }

    fn push_spans(&mut self, Spans(mut spans): Spans) {
        let Spans(last) = self.r#final.get_or_insert_with(Default::default);
        last.append(&mut spans);
    }

    pub fn clear(&mut self) {
        self.full.clear();
        self.r#final.take();
    }

    pub fn append(&mut self, mut lines: Lines) -> usize {
        let dropped = lines.full.drain(..).fold(0, |n, l| n + self.push_line(l));
        if let Some(l) = lines.r#final.take() {
            self.push_spans(l)
        }
        dropped
    }

    pub fn set_capacity(&mut self, capacity: usize) {
        self.capacity = capacity;
        self.drop_extra();
    }

    pub fn len(&self) -> usize {
        let full_len = self.full.len();
        let final_len = self.r#final.as_ref().map(|_| 1).unwrap_or_default();
        full_len + final_len
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    pub fn slice<R>(&self, range: R) -> impl Iterator<Item = &[Span]>
    where
        R: RangeBounds<usize>,
    {
        LinesRange {
            complete: self.full.iter().map(|Line(l)| l.as_slice()),
            last: self.r#final.as_ref(),
            bounds: range,
        }
        .line_spans()
    }
}
