use std::ops::{
    Bound,
    RangeBounds,
};

use mudrs_extension::text::{
    Span,
    Spans,
};

#[derive(Debug, PartialEq)]
pub struct LinesRange<'r, I, R>
where
    I: ExactSizeIterator + Iterator<Item = &'r [Span]> + 'r,
    R: RangeBounds<usize>,
{
    pub(crate) complete: I,
    pub(crate) last: Option<&'r Spans>,
    pub(crate) bounds: R,
}

impl<'r, I, R> LinesRange<'r, I, R>
where
    I: ExactSizeIterator + Iterator<Item = &'r [Span]> + 'r,
    R: RangeBounds<usize>,
{
    pub fn line_spans(mut self) -> impl Iterator<Item = &'r [Span]> {
        let complete_len = self.complete.len();

        // Advance past the first excluded item.
        let first = match self.bounds.start_bound() {
            Bound::Excluded(i) => {
                self.complete.nth(*i);
                *i + 1
            }
            Bound::Included(i) => {
                if *i > 0 {
                    self.complete.nth(*i - 1);
                }
                *i
            }
            _ => 0,
        };

        let last = match self.bounds.end_bound() {
            Bound::Excluded(i) => *i,
            Bound::Included(i) => *i + 1,
            Bound::Unbounded => complete_len + self.last.is_some() as usize,
        };

        let incomplete = self
            .last
            .into_iter()
            .map(|Spans(s)| s.as_slice())
            .filter(move |_| last > complete_len);

        let complete = self.complete.take(last - first);

        complete.chain(incomplete)
    }
}
