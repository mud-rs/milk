use std::io;

use alacritty_terminal::ansi::{
    Attr,
    Color as TColor,
    Handler,
    NamedColor,
    Processor,
};
use mudrs_extension::{
    rune::termcolor,
    text::{
        Color,
        Line,
        Lines,
        Span,
        Spans,
        Style,
    },
};
use tracing::{
    info,
    instrument,
    trace,
};

#[derive(Debug)]
struct AnsiHandler {
    lines: Lines,

    style: Style,

    // Two characters of look-behind for mid-line CR handling
    // We don't allow those, so we'll insert a newline if we encounter one.
    prev: char,
    prevprev: char,
}

pub struct AnsiProcessor {
    handler: AnsiHandler,
    processor: Processor,
}

impl Default for AnsiProcessor {
    fn default() -> Self {
        Self::new()
    }
}

impl AnsiProcessor {
    pub fn new() -> Self {
        AnsiProcessor {
            processor: Processor::new(),
            handler: AnsiHandler::new(),
        }
    }

    pub fn append(&mut self, bytes: &[u8]) {
        let handler = &mut self.handler;

        let processor = &mut self.processor;
        for b in bytes {
            processor.advance(handler, *b)
        }
    }

    pub fn take(&mut self) -> Lines {
        std::mem::take(&mut self.handler.lines)
    }
}

impl AnsiHandler {
    fn new() -> AnsiHandler {
        AnsiHandler {
            lines: Default::default(),
            style: Style::default(),
            prev: 0 as char,
            prevprev: 0 as char,
        }
    }

    #[instrument(level = "trace", skip(self))]
    fn new_line(&mut self) {
        let complete = if let Some(Spans(spans)) = self.lines.r#final.take() {
            Line(spans)
        } else {
            Line::default()
        };
        self.lines.full.push(complete);
        self.lines.r#final = Some(Spans::default());
    }

    #[instrument(level = "trace", skip(self))]
    fn apply_style(&mut self) {
        let Spans(last_line) = self.lines.r#final.get_or_insert_with(Default::default);
        if last_line.is_empty() {
            return;
        }
        let last_span = last_line.last_mut().unwrap();
        if last_span.content.is_empty() {
            last_span.style = self.style;
            return;
        }
        last_line.push(Span {
            style: self.style,
            ..Default::default()
        });
    }

    #[instrument(level = "trace", skip(self))]
    fn shift_prev(&mut self, c: char) {
        self.prevprev = self.prev;
        self.prev = c;
    }
}

impl Handler for AnsiHandler {
    #[instrument(level = "trace", skip(self))]
    fn input(&mut self, c: char) {
        // If we got a carriage return *without* a linefeed, go ahead and insert
        // one.
        if self.prev == '\r' && self.prevprev != '\n' {
            info!("fabricating new line");
            self.new_line();
        }
        self.shift_prev(c);
        let Spans(l) = self.lines.r#final.get_or_insert_with(Default::default);
        if l.is_empty() {
            l.push(Span {
                content: Default::default(),
                style: self.style,
            });
        }
        l.last_mut().unwrap().content.push(c);
    }

    #[instrument(level = "trace", skip_all)]
    fn newline(&mut self) {
        self.new_line();
    }

    #[instrument(level = "trace", skip_all)]
    fn linefeed(&mut self) {
        trace!("got 'linefeed'");
        self.shift_prev('\n');
        self.new_line();
    }

    #[instrument(level = "trace", skip_all)]
    fn carriage_return(&mut self) {
        self.shift_prev('\r');
    }

    fn terminal_attribute(&mut self, attr: Attr) {
        match attr {
            Attr::Reset => self.style = Style::default(),
            Attr::Foreground(color) => {
                self.style.fg = to_tui_color(color);
            }
            Attr::Background(color) => {
                self.style.bg = to_tui_color(color);
            }
            Attr::Bold => self.style.bold = true,
            Attr::Italic => self.style.italic = true,
            Attr::Dim => self.style.dim = true,
            Attr::Underline => self.style.underlined = true,
            Attr::BlinkSlow => self.style.slow_blink = true,
            Attr::BlinkFast => self.style.rapid_blink = true,
            Attr::Reverse => self.style.reversed = true,
            Attr::Hidden => self.style.hidden = true,
            Attr::Strike => self.style.crossed_out = true,
            Attr::CancelBold => self.style.bold = false,
            Attr::CancelItalic => self.style.italic = false,
            Attr::CancelBoldDim => {
                let mut mods = &mut self.style;
                mods.bold = false;
                mods.dim = false;
            }
            Attr::CancelUnderline => self.style.underlined = false,
            Attr::CancelBlink => {
                let mods = &mut self.style;
                mods.rapid_blink = false;
                mods.slow_blink = false;
            }
            Attr::CancelReverse => self.style.reversed = false,
            Attr::CancelHidden => self.style.hidden = false,
            Attr::CancelStrike => self.style.crossed_out = false,
            _ => {}
        }
        self.apply_style()
    }
}

fn to_tui_color(color: TColor) -> Option<Color> {
    let named: NamedColor = match color {
        TColor::Named(named) => named,
        TColor::Indexed(id) => {
            return Color::Indexed(id).into();
        }
        TColor::Spec(rgb) => {
            return Color::Rgb(rgb.r, rgb.g, rgb.b).into();
        }
    };

    Some(match named {
        NamedColor::BrightBlack => Color::BrightBlack,
        NamedColor::Black => Color::Black,
        NamedColor::BrightRed => Color::BrightRed,
        NamedColor::BrightWhite => Color::BrightWhite,
        NamedColor::BrightGreen => Color::BrightGreen,
        NamedColor::BrightYellow => Color::BrightYellow,
        NamedColor::BrightBlue => Color::BrightBlue,
        NamedColor::BrightMagenta => Color::BrightMagenta,
        NamedColor::BrightCyan => Color::BrightCyan,
        NamedColor::Red => Color::Red,
        NamedColor::White => Color::White,
        NamedColor::Green => Color::Green,
        NamedColor::Yellow => Color::Yellow,
        NamedColor::Blue => Color::Blue,
        NamedColor::Magenta => Color::Magenta,
        NamedColor::Cyan => Color::Cyan,
        _ => return None,
    })
}

impl io::Write for AnsiProcessor {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        self.append(buf);
        Ok(buf.len())
    }
    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}

impl termcolor::WriteColor for AnsiProcessor {
    fn supports_color(&self) -> bool {
        true
    }
    fn is_synchronous(&self) -> bool {
        true
    }
    fn set_color(&mut self, spec: &termcolor::ColorSpec) -> io::Result<()> {
        if spec.reset() {
            self.handler.style = Style::default();
        }
        self.handler.style.bold = spec.bold();
        self.handler.style.underlined = spec.underline();
        self.handler.style.dim = spec.dimmed();
        self.handler.style.italic = spec.italic();
        if let Some(color) = spec.fg() {
            self.handler.style.fg = Color::try_from(*color).ok();
        }
        if let Some(color) = spec.bg() {
            self.handler.style.bg = Color::try_from(*color).ok();
        }
        if let Some(color) = self.handler.style.bg.as_mut() {
            *color = color.intense(spec.intense());
        }
        if let Some(color) = self.handler.style.fg.as_mut() {
            *color = color.intense(spec.intense());
        }
        self.handler.apply_style();
        Ok(())
    }
    fn reset(&mut self) -> io::Result<()> {
        self.handler.style = Style::default();
        self.handler.apply_style();
        Ok(())
    }
}
