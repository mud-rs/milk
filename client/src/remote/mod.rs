use std::{
    collections::VecDeque,
    io,
    sync::{
        atomic::{
            AtomicBool,
            Ordering,
        },
        Arc,
    },
    time::Duration,
};

use anyhow::{
    Context,
    Error,
};
use bytes::{
    Buf,
    BufMut,
    BytesMut,
};
use futures::{
    pin_mut,
    prelude::*,
};
use mudrs_extension::{
    bus::{
        Bus,
        PublishEvent,
        SharedClient,
    },
    interface::Mode,
    remote::{
        RemoteIn,
        RemoteOut,
    },
    text::{
        Color,
        Line,
        Lines,
        Spans,
        Style,
    },
};
use mudrs_text::AnsiProcessor;
use parking_lot::Mutex;
use tellem::{
    Cmd,
    Event,
    KnownOpt,
    Opt,
};
use tokio::{
    net::{
        TcpStream,
        ToSocketAddrs,
    },
    sync::{
        mpsc,
        oneshot,
    },
};
use tokio_stream::wrappers::UnboundedReceiverStream;
use tracing::{
    debug,
    error,
    instrument,
};

#[derive(Debug, Clone)]
pub struct TelnetRemote {
    inner: Arc<Mutex<TelnetInner>>,
    sender: mpsc::UnboundedSender<SendReq>,
}

#[derive(Debug)]
struct SendReq {
    event: tellem::Event,
    resp: Option<oneshot::Sender<Result<(), io::Error>>>,
}

#[derive(Default, Debug)]
struct TelnetInner {
    // Backlog of messages that haven't been sent to a client yet.
    buffer: VecDeque<RemoteIn>,
    receivers: Vec<mpsc::UnboundedSender<RemoteIn>>,
}

fn close_receivers(inner: &Mutex<TelnetInner>) {
    inner.lock().receivers.drain(..);
}

#[instrument(skip(inner), level = "trace")]
fn send_inner(inner: &Mutex<TelnetInner>, msg: RemoteIn) {
    let mut lock = inner.lock();
    if lock.receivers.is_empty() {
        lock.buffer.push_back(msg);
    } else {
        let mut bad = vec![];
        for (i, r) in lock.receivers.iter_mut().enumerate() {
            if r.send(msg.clone()).is_err() {
                bad.push(i);
            }
        }
        for i in bad.iter().rev().cloned() {
            lock.receivers.remove(i);
        }
    }
}

impl TelnetRemote {
    pub async fn new(addr: impl ToSocketAddrs) -> Result<Self, io::Error> {
        let conn = TcpStream::connect(addr).await?;
        let conn = tellem::TnConn::start(conn);
        let (mut remote_sender, mut remote_receiver) = conn.split();

        let (sender, mut receiver) = mpsc::unbounded_channel();

        let inner = Arc::new(Mutex::new(Default::default()));

        let this = Self {
            inner: inner.clone(),
            sender: sender.clone(),
        };

        remote_sender
            .send(Event::Negotiation(Cmd::DO, Opt::Known(KnownOpt::EOR)))
            .await?;

        tokio::spawn(async move {
            loop {
                debug!("waiting for input event");
                let SendReq { event, resp } = if let Some(ev) = receiver.recv().await {
                    ev
                } else {
                    debug!("no more input events");
                    return;
                };

                debug!(?event, "got event to send to remote");
                if let Err(err) = remote_sender.send(event).await {
                    error!(?err, "error sending event to remote");
                    let _ = resp.map(|ch| ch.send(Err(err)));
                    return;
                }
                if let Some(resp) = resp {
                    let res = remote_sender.flush().await;
                    let _ = resp.send(res);
                }
            }
        });

        tokio::spawn(
            {
                let inner = inner.clone();
                let sender = sender.clone();
                async move {
                    let mut ansi_processor = AnsiProcessor::new();
                    while let Some(event) = remote_receiver.try_next().await? {
                        handle_event(event, &inner, &sender, &mut ansi_processor)?;
                    }
                    Ok(())
                }
            }
            .then({
                let inner = inner.clone();
                let sender = sender.clone();
                move |_: Result<(), anyhow::Error>| async move {
                    let (tx, rx) = oneshot::channel();
                    let _ = sender.send(SendReq {
                        event: Event::Data(b""[..].into()),
                        resp: Some(tx),
                    });
                    let _ = rx.await;
                    close_receivers(&inner)
                }
            }),
        );

        Ok(this)
    }
}

fn handle_event(
    event: Event,
    inner: &Mutex<TelnetInner>,
    sender: &mpsc::UnboundedSender<SendReq>,
    ansi_processor: &mut AnsiProcessor,
) -> Result<(), anyhow::Error> {
    match event {
        Event::Negotiation(Cmd::WONT, Opt::Known(KnownOpt::ECHO)) => {
            send_inner(inner, RemoteIn::Echo(true));
        }
        Event::Negotiation(Cmd::WILL, Opt::Known(KnownOpt::ECHO)) => {
            sender.send(SendReq {
                event: Event::Negotiation(Cmd::DO, Opt::Known(KnownOpt::ECHO)),
                resp: None,
            })?;
            send_inner(inner, RemoteIn::Echo(false));
        }
        Event::Negotiation(
            Cmd::WILL,
            opt @ Opt::Known(KnownOpt::MSDP | KnownOpt::MSSP | KnownOpt::MSP | KnownOpt::GMCP),
        ) => {
            sender.send(SendReq {
                event: Event::Negotiation(Cmd::DO, opt),
                resp: None,
            })?;

            if matches!(opt, Opt::Known(KnownOpt::MSDP)) {
                let _msg = BytesMut::new();
                for t in ["COMMANDS", "REPORTABLE_VARIABLES"] {
                    let mut msg = BytesMut::new();
                    msg.put_u8(0x01);
                    msg.put(&b"LIST"[..]);
                    msg.put_u8(0x02);
                    msg.put(t.as_bytes());
                    sender.send(SendReq {
                        event: Event::Subnegotiation(opt, msg),
                        resp: None,
                    })?;
                }
            }
        }
        Event::Subnegotiation(_opt @ Opt::Known(KnownOpt::GMCP), bs) => {
            debug!(?bs, "got gmcp");
        }
        Event::Subnegotiation(opt @ Opt::Known(KnownOpt::MSDP), mut bs) => {
            let ty = bs.get_u8();
            if ty != 0x01 {
                debug!(?bs, "invalid msdp message");
                return Ok(());
            } else if bs.is_empty() {
                debug!(?bs, "msdp var message with no body");
                return Ok(());
            } else {
                let mut b = bs.get_u8();
                let mut var_name = vec![];
                while b != 0x02 {
                    var_name.push(b);
                    if bs.is_empty() {
                        break;
                    }
                    b = bs.get_u8();
                }
                let var_name = String::from_utf8_lossy(&var_name);
                let mut values = vec![];
                let mut current = vec![];
                while !bs.is_empty() {
                    b = bs.get_u8(); // Skip the 0x02 byte
                    current.clear();
                    while b != 0x02 {
                        current.push(b);
                        if bs.is_empty() {
                            break;
                        }
                        b = bs.get_u8();
                    }
                    values.push(String::from_utf8_lossy(&current).into_owned());
                }

                if var_name == "REPORTABLE_VARIABLES" {
                    let mut msg = BytesMut::new();
                    msg.put_u8(0x01); // MSDP VAR
                    msg.put(&b"REPORT"[..]);
                    for v in &values {
                        msg.put_u8(0x02); // MDSP VAL
                        msg.put(v.as_bytes());
                    }
                    debug!(target: "telnet_unhandled", msg = ?String::from_utf8_lossy(msg.as_ref()), "sending msdp request");
                    sender.send(SendReq {
                        event: Event::Subnegotiation(opt, msg),
                        resp: None,
                    })?;
                } else {
                    debug!(target: "telnet_unhandled", %var_name, ?values, "got msdp values");
                }
            }
        }
        Event::Negotiation(
            Cmd::DO,
            opt @ Opt::Known(KnownOpt::TERMINAL_TYPE | KnownOpt::CHARSET),
        ) => {
            sender.send(SendReq {
                event: Event::Negotiation(Cmd::WILL, opt),
                resp: None,
            })?;
        }
        Event::Subnegotiation(opt @ Opt::Known(KnownOpt::CHARSET), mut bs) => {
            if bs.get_u8() == 0x01 {
                bs.get_u8(); // Skip space
                let charset = String::from_utf8_lossy(bs.as_ref());
                if charset.to_lowercase() == "utf-8" {
                    let mut msg = BytesMut::new();
                    msg.put_u8(0x02);
                    msg.put_u8(b' ');
                    msg.put(charset.as_bytes());
                    sender.send(SendReq {
                        event: Event::Subnegotiation(opt, msg),
                        resp: None,
                    })?;
                }
            }
        }
        // Handle requests to send our terminal type information
        Event::Subnegotiation(Opt::Known(KnownOpt::TERMINAL_TYPE), bs) if bs.as_ref() == [0x01] => {
            let mut msg_bytes = BytesMut::new();
            msg_bytes.put_u8(0x00);
            msg_bytes.put(&b"xterm-256color"[..]);
            sender.send(SendReq {
                event: Event::Subnegotiation(Opt::Known(KnownOpt::TERMINAL_TYPE), msg_bytes),
                resp: None,
            })?;
        }
        Event::Data(bytes) => {
            ansi_processor.append(&bytes);
            send_inner(inner, RemoteIn::Lines(ansi_processor.take()));
        }
        event => {
            debug!(target: "telnet_unhandled", ?event, "unhandled telnet event");
            send_inner(inner, RemoteIn::Eor(true));
        }
    }

    Ok(())
}

impl TelnetRemote {
    #[instrument(skip(self), level = "trace")]
    fn receive(&self) -> UnboundedReceiverStream<RemoteIn> {
        let (tx, rx) = mpsc::unbounded_channel();
        let mut lock = self.inner.lock();
        // If there were no receivers previously, flush our buffer to the new
        // one.
        if lock.receivers.is_empty() {
            for msg in lock.buffer.drain(..) {
                let _ = tx.send(msg);
            }
        }
        lock.receivers.push(tx);
        UnboundedReceiverStream::new(rx)
    }

    #[instrument(skip(self), level = "trace")]
    async fn send(&self, req: RemoteOut) -> Result<(), Error> {
        let ev = match req {
            RemoteOut::Command(mut s) => {
                if !s.ends_with("\r\n") {
                    s.push_str("\r\n");
                }
                Event::Data(s.as_str().into())
            }
            RemoteOut::Gmcp { .. } => {
                unimplemented!()
            }
            RemoteOut::Msdp { .. } => {
                unimplemented!()
            }
        };
        let (tx, rx) = oneshot::channel();
        debug!(?ev, "sending event to remote");
        self.sender.send(SendReq {
            event: ev,
            resp: tx.into(),
        })?;
        rx.await.context("the remote is not connected")??;
        Ok(())
    }

    pub async fn start(self, name: &str, bus: impl Bus) -> Result<(), Error> {
        let mut client = bus.get_client(&format!("telnet.{name}")).await?;
        client
            .subscribe(mudrs_extension::bus::Topic::RemoteOut)
            .await?;
        let events_out = client.events().unwrap();
        let client = SharedClient::new(client);
        let mut events_in = self.receive();
        let disconnected = Arc::new(AtomicBool::new(false));

        tokio::spawn({
            let client = client.clone();
            let disconnected = disconnected.clone();
            async move {
                let mut trailing: Option<Spans> = None;
                while let Some(mut msg) = if trailing.is_some() {
                    tokio::time::timeout(Duration::from_secs_f32(0.5), events_in.next())
                        .await
                        .unwrap_or_else(|_| {
                            debug!("timeout waiting for end of line");
                            Some(RemoteIn::Lines(Lines::new("\r\n", Default::default())))
                        })
                } else {
                    events_in.next().await
                } {
                    let mut lines = Lines::default();

                    if let RemoteIn::Lines(ref mut new) = msg {
                        debug!(?trailing, ?new, "got lines from remote");
                        lines.r#final = trailing.take();
                        lines.append(std::mem::take(new));
                        trailing = lines.r#final.take();
                        *new = lines;
                    } else if let Some(Spans(s)) = trailing.take() {
                        // If we got anything but data and we had a trailing
                        // line, assume it's complete enough.
                        lines.full.push(Line(s));
                        client
                            .publish(mudrs_extension::bus::PublishEvent::RemoteIn(
                                RemoteIn::Lines(lines),
                            ))
                            .await?;
                    }

                    if trailing
                        .as_ref()
                        .map(|t| t.iter().fold(0, |n, s| n + s.content.len()))
                        .unwrap_or(0)
                        == 0
                    {
                        trailing.take();
                    }

                    debug!(event = ?msg, "publish remote_in event");

                    client
                        .publish(mudrs_extension::bus::PublishEvent::RemoteIn(msg))
                        .await?;

                    debug!("waiting for next telnet event");
                }
                disconnected.store(true, Ordering::Relaxed);
                client
                    .append(
                        &[(0, Mode::Primary)],
                        Lines::new(
                            "Remote connection closed\n",
                            Style::default().fg(Color::Red),
                        ),
                    )
                    .await?;
                Ok::<(), Error>(())
            }
        });

        tokio::spawn(async move {
            let events = events_out.filter_map(|e| async move {
                use mudrs_extension::bus::Event;
                match e {
                    Ok(Event::Publish(PublishEvent::RemoteOut(e))) => Some(e),
                    _ => None,
                }
            });

            pin_mut!(events);
            while let Some(event) = events.next().await {
                if disconnected.load(Ordering::Relaxed) {
                    client
                        .append(
                            &[(0, Mode::Primary)],
                            Lines::new("Remote not connected\n", Style::default().fg(Color::Red)),
                        )
                        .await?;
                } else {
                    self.send(event).await?;
                }
            }
            Ok::<_, Error>(())
        });
        Ok(())
    }
}
