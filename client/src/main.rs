use std::{
    ffi::OsStr,
    io::stdout,
    path::{
        Path,
        PathBuf,
    },
    sync::Arc,
};

use anyhow::bail;
use clap::Parser;
use crossterm::{
    event::{
        DisableMouseCapture,
        EnableMouseCapture,
    },
    execute,
    terminal::{
        EnterAlternateScreen,
        LeaveAlternateScreen,
    },
};
use futures::prelude::*;
use mudrs_extension::bus::{
    AnyBus,
    ExternalBus,
    InternalBus,
};
use mudrs_milk::{
    config::{
        self,
        Config,
        Profile,
    },
    interface,
    logger::Logger,
    remote::TelnetRemote,
};
use tracing::info;
#[allow(unused_imports)]
use tracing::{
    debug,
    error,
    warn,
};
use tracing_subscriber::fmt::format::FmtSpan;
use tui::backend::{
    Backend,
    CrosstermBackend,
};

#[derive(Parser, Debug, Clone)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Config file containing profiles.
    #[clap(long, value_parser, default_value_os_t = config_dir().join("config.toml"))]
    config: PathBuf,
    /// Profile to load.
    #[clap(value_parser, default_value_t = String::from("default"))]
    profile: String,

    #[clap(flatten)]
    overrides: config::Profile,

    /// Disable the script engine.
    #[clap(long, value_parser)]
    no_script: bool,
    /// Disable the remote connection.
    #[clap(long, value_parser)]
    no_remote: bool,
    /// Disable the tui.
    #[clap(long, value_parser)]
    no_ui: bool,
    /// Disable the listener.
    #[clap(long, value_parser)]
    no_listen: bool,
}

fn config_dir() -> PathBuf {
    if let Ok(dir) = std::env::var("XDG_CONFIG_HOME") {
        Path::new(&dir).join("milk")
    } else {
        home::home_dir()
            .map(|mut h| {
                h.push(".config");
                h.push("milk");
                h
            })
            .unwrap_or_else(|| PathBuf::from("."))
    }
}

fn default_state_dir(name: &str) -> PathBuf {
    if let Ok(dir) = std::env::var("XDG_STATE_HOME") {
        Path::new(&dir).join("milk")
    } else {
        home::home_dir()
            .map(|mut h| {
                h.push(".local");
                h.push("state");
                h.push("milk");
                h
            })
            .unwrap_or_else(|| PathBuf::from("."))
    }
    .join(name)
}

fn main() -> anyhow::Result<()> {
    let args = Args::parse();

    let config_file = args.config;

    std::fs::create_dir_all(config_file.parent().unwrap())?;

    let config: Config = std::fs::read(config_file)
        .map_err(anyhow::Error::from)
        .and_then(|config_bytes| toml::from_slice(&config_bytes).map_err(anyhow::Error::from))
        .unwrap_or_else(|_| Config::default());

    let mut profile = config
        .get_profile(&args.profile)
        .unwrap_or_default()
        .with_overrides(args.overrides)
        .expand_env();

    // Default to UI 0 assuming no-ui wasn't requested.
    if args.no_ui {
        profile.ui.take();
    } else {
        profile.ui = profile.ui.take().or(Some(0));
    }

    if args.no_remote {
        profile.remote.take();
    }
    if args.no_listen {
        profile.listen.take();
    }
    if args.no_script {
        profile.script.take();
    }

    // Disable the listener if we're connecting to another instance.
    if profile.connect.is_some() {
        profile.listen.take();
    }

    let state_dir = profile
        .state
        .clone()
        .unwrap_or_else(|| default_state_dir(&args.profile));

    std::fs::create_dir_all(&state_dir)?;

    let (non_blocking, _guard) = if profile.ui.is_some() {
        let file_appender = tracing_appender::rolling::daily(&state_dir, "milk.log");
        tracing_appender::non_blocking(file_appender)
    } else {
        tracing_appender::non_blocking(stdout())
    };
    let filter = tracing_subscriber::EnvFilter::from_default_env();

    tracing_subscriber::fmt()
        .pretty()
        .with_writer(non_blocking)
        .with_span_events(FmtSpan::NEW | FmtSpan::CLOSE)
        .with_env_filter(filter)
        .init();

    std::panic::set_hook(Box::new(move |info| {
        let trace = backtrace::Backtrace::new();
        tracing::error!(?trace, ?info, "something panicked");
    }));

    let backend = if profile.ui.is_some() && !args.no_ui {
        // Terminal initialization
        crossterm::terminal::enable_raw_mode()?;
        execute!(stdout(), EnterAlternateScreen, EnableMouseCapture)?;
        let buffered = std::io::BufWriter::with_capacity(5_000_000, stdout());
        CrosstermBackend::new(buffered).into()
    } else {
        None
    };

    let rt = tokio::runtime::Runtime::new()?;

    let local_set = tokio::task::LocalSet::new();
    let res = local_set.block_on(
        &rt,
        async_main(&args.profile, profile.clone(), state_dir, backend),
    );

    if profile.ui.is_some() {
        info!("disabling raw mode");
        execute!(stdout(), DisableMouseCapture, LeaveAlternateScreen)?;
        crossterm::terminal::disable_raw_mode()?;
    }

    drop(rt);

    info!("returning");

    res
}

async fn async_main(
    name: &str,
    profile: Profile,
    state_dir: impl AsRef<Path> + 'static,
    backend: Option<impl Backend + Send + 'static>,
) -> anyhow::Result<()> {
    let now = chrono::Local::now();

    let bus: Arc<AnyBus> = if let Some(addr) = profile.connect {
        Arc::new(ExternalBus::new(addr)?.into())
    } else {
        let mut bus = InternalBus::new();
        if let Some(addr) = profile.listen {
            bus.listen(addr, None).await?;
        }

        let bus: Arc<AnyBus> = Arc::new(bus.into());

        let logger = Logger {
            path: state_dir
                .as_ref()
                .join(format!("log.json.{}", now.timestamp())),
        };

        logger.start(bus.clone()).await?;

        bus
    };

    let end = if let Some(ui) = profile.ui {
        let tui = interface::TuInterface::new(backend.expect("tui backend"), &state_dir, ui)?;
        let events = tui.receive().fold((), |_, _| async {});
        tui.start(bus.clone()).await?;
        events.boxed()
    } else {
        futures::future::pending::<()>().boxed()
    };

    if let Some(path) = profile.script {
        let ext = if let Some(ext) = path.extension().and_then(OsStr::to_str) {
            ext
        } else {
            bail!("missing script extension");
        };
        tokio::task::spawn_local(match ext {
            "ts" => {
                let task = mudrs_script_deno::run(bus.clone(), path, state_dir).await?;
                task.boxed_local()
            }
            "rn" => mudrs_script_rune::run(bus.clone(), path, state_dir).boxed_local(),
            _ => {
                bail!("unrecognized script extension: {ext}");
            }
        });
    }

    if let Some(remote) = profile.remote {
        let remote = TelnetRemote::new(remote).await?;
        remote.start(name, bus.clone()).await?;
    }

    end.await;
    drop(bus);
    Ok(())
}
