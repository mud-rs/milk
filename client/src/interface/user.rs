use std::thread;

use crossbeam::channel::{
    unbounded,
    Receiver,
};
use crossterm::{
    event,
    event::Event,
};
use rustyline::tty::VirtInput;
use tracing::debug;

use crate::interface::scrollback::ScrollbackHandle;

pub struct User {
    _input_handle: thread::JoinHandle<()>,
    events: Receiver<Event>,
}

impl Default for User {
    fn default() -> Self {
        User::new()
    }
}

impl User {
    pub fn new() -> Self {
        let (tx, rx) = unbounded();
        let input_handle = {
            thread::spawn(move || {
                let _span = tracing::trace_span!("read_user_input").entered();
                loop {
                    let event = match event::read() {
                        Ok(e) => e,
                        Err(err) => {
                            tracing::error!(?err, "error reading event from terminal");
                            continue;
                        }
                    };

                    // tracing::trace!(?event, "got input event");

                    if let Err(err) = tx.send(event) {
                        tracing::error!(?err, "error sending user event");
                        return;
                    }
                }
            })
        };

        Self {
            _input_handle: input_handle,
            events: rx,
        }
    }

    pub fn dispatch_events(self, mut rl_input: VirtInput, scrollback: ScrollbackHandle) {
        thread::spawn(move || {
            let _span = tracing::trace_span!("dispatch_events").entered();
            for event in self.events.iter() {
                debug!(target: "user::dispatch", ?event, "got event");
                if let Some(rl_event) = to_rl_event(event) {
                    debug!(target: "user::dispatch", ?rl_event, "sending key to rustyline");
                    let _ = rl_input.send_key(rl_event);
                }
                scrollback.write().handle_user_event(&event);
            }
        });
    }
}

fn to_rl_event(event: Event) -> Option<rustyline::KeyEvent> {
    use crossterm::event::{
        KeyCode as CCode,
        KeyEvent as CKey,
        KeyModifiers as CMods,
    };
    use rustyline::{
        KeyCode as RCode,
        KeyEvent as RKey,
        Modifiers as RMods,
    };
    Some(match event {
        Event::Key(CKey { code, modifiers }) => {
            let rl_code = match code {
                CCode::Backspace => RCode::Backspace,
                CCode::Enter => RCode::Enter,
                CCode::Left => RCode::Left,
                CCode::Right => RCode::Right,
                CCode::Up => RCode::Up,
                CCode::Down => RCode::Down,
                CCode::Home => RCode::Home,
                CCode::End => RCode::End,
                CCode::PageUp => RCode::PageUp,
                CCode::PageDown => RCode::PageDown,
                CCode::Tab => RCode::Tab,
                CCode::BackTab => RCode::BackTab,
                CCode::Delete => RCode::Delete,
                CCode::Insert => RCode::Insert,
                CCode::F(b) => RCode::F(b),
                CCode::Char(c) => RCode::Char(c),
                CCode::Null => RCode::Null,
                CCode::Esc => RCode::Esc,
            };
            let mut rl_mods = RMods::empty();
            rl_mods.set(RMods::SHIFT, modifiers.contains(CMods::SHIFT));
            rl_mods.set(RMods::CTRL, modifiers.contains(CMods::CONTROL));
            rl_mods.set(RMods::ALT, modifiers.contains(CMods::ALT));

            RKey::normalize(RKey(rl_code, rl_mods))
        }
        _ => return None,
    })
}
