pub mod input;
pub mod renderer;
pub mod scrollback;
pub mod user;

use std::{
    collections::VecDeque,
    path::Path,
    sync::{
        atomic::{
            AtomicBool,
            Ordering,
        },
        Arc,
    },
    thread,
};

use anyhow::Error;
use futures::{
    pin_mut,
    StreamExt,
};
use linked_hash_set::LinkedHashSet;
use mudrs_extension::{
    bus::{
        Bus,
        Event,
        PublishEvent,
    },
    interface::{
        Mode,
        Output,
        UiIn,
        UiOut,
    },
    text::Lines,
};
use parking_lot::{
    Mutex,
    RwLock,
};
use rustyline::{
    Config,
    Editor,
    EventHandler,
    KeyCode,
    KeyEvent,
    Modifiers,
};
use tokio::sync::mpsc;
use tokio_stream::wrappers::UnboundedReceiverStream;
use tracing::{
    debug,
    instrument,
    warn,
};
use tui::backend::Backend;

use self::{
    renderer::{
        NamedRenderRequester,
        Renderer,
    },
    scrollback::ScrollbackBuffer,
    user::User,
};

pub struct TuInterface {
    scrollback: Arc<RwLock<ScrollbackBuffer>>,
    completions: Arc<RwLock<LinkedHashSet<String>>>,
    input: Arc<Mutex<TuiInner>>,
    echo: Arc<AtomicBool>,
    prompt: Arc<RwLock<Lines>>,
    prompt_requester: NamedRenderRequester,
    id: u32,
}

#[derive(Default, Debug)]
struct TuiInner {
    id: u32,
    buffer: VecDeque<UiIn>,
    previous: Option<String>,
    receivers: Vec<mpsc::UnboundedSender<UiIn>>,
}

impl TuiInner {
    pub fn close_receivers(&mut self) {
        self.receivers.drain(..);
    }

    pub fn send_reload(&mut self) {
        let ev = UiIn::Reload { id: self.id };
        self.send_event(ev)
    }

    pub fn send_event(&mut self, ev: UiIn) {
        if self.receivers.is_empty() {
            self.buffer.push_back(ev);
        } else {
            let mut bad = vec![];
            for (i, r) in self.receivers.iter_mut().enumerate() {
                if r.send(ev.clone()).is_err() {
                    bad.push(i);
                }
            }
            for i in bad.iter().rev().cloned() {
                self.receivers.remove(i);
            }
        }
    }

    pub fn send(&mut self, cmd: String) {
        let ev = UiIn::Command {
            id: self.id,
            command: cmd.clone(),
        };
        self.send_event(ev);
        self.previous = Some(cmd);
    }

    pub fn send_prev(&mut self) {
        if let Some(msg) = self.previous.take() {
            self.send(msg);
        }
    }
}

impl TuInterface {
    pub fn new(
        backend: impl Backend + Send + 'static,
        state_dir: impl AsRef<Path>,
        id: u32,
    ) -> Result<Self, anyhow::Error> {
        let echo = Arc::new(AtomicBool::new(true));
        let (rrequester, rrequests) = renderer::requester();
        let (rl_input, rl_output) = rustyline::tty::init(Some(Box::new({
            let renderer = rrequester.with_name("rustyline");
            move || {
                renderer.send_request();
            }
        })));
        let scrollback = Arc::new(RwLock::new(ScrollbackBuffer::new(rrequester.clone())));
        let prompt = Arc::new(RwLock::new(Default::default()));
        Renderer::new(
            backend,
            rl_output,
            scrollback.clone(),
            echo.clone(),
            prompt.clone(),
        )?
        .run(rrequests);
        User::new().dispatch_events(rl_input, scrollback.clone());
        let input = Arc::new(Mutex::new(TuiInner {
            id,
            ..Default::default()
        }));
        let completions = Arc::new(RwLock::new(LinkedHashSet::new()));
        read_lines(state_dir, input.clone(), completions.clone(), echo.clone());
        Ok(TuInterface {
            scrollback,
            completions,
            input,
            echo,
            prompt,
            prompt_requester: rrequester.with_name("prompt"),
            id,
        })
    }

    pub async fn start(self, bus: impl Bus) -> Result<(), Error> {
        let mut client = bus.get_client(&format!("tui.{}", self.id)).await?;
        client.subscribe(mudrs_extension::bus::Topic::UiOut).await?;
        let mut events_in = self.receive();
        let events_out = client.events().unwrap();

        tokio::spawn(async move {
            while let Some(event) = events_in.next().await {
                client
                    .publish(mudrs_extension::bus::PublishEvent::UiIn(event))
                    .await?;
            }
            Ok::<(), Error>(())
        });

        tokio::spawn(async move {
            let events = events_out.filter_map(|e| async move {
                match e {
                    Ok(Event::Publish(PublishEvent::UiOut(e))) => Some(e),
                    _ => None,
                }
            });

            pin_mut!(events);
            while let Some(event) = events.next().await {
                match event {
                    UiOut::Append { lines, outputs } => {
                        self.append(lines, outputs);
                    }
                    UiOut::SetContent { lines, output_id } => {
                        self.set_content(lines, output_id);
                    }
                    UiOut::SetPrompt { lines, output_id } => {
                        self.set_prompt(lines, output_id);
                    }
                    UiOut::SetEcho(e) => {
                        self.set_echo(e);
                    }
                }
            }
            self.append(
                Lines::new("Bus disappeared", Default::default()),
                vec![Output {
                    id: self.id,
                    mode: Mode::Primary,
                }],
            );
        });
        Ok(())
    }
}

impl TuInterface {
    #[instrument(level = "trace", skip(self))]
    fn set_echo(&self, value: bool) {
        self.echo.store(value, Ordering::Relaxed);
    }

    #[instrument(level = "trace", skip(self))]
    fn set_prompt(&self, lines: Lines, output_id: u32) {
        if output_id != self.id {
            return;
        }
        *self.prompt.write() = lines;
        self.prompt_requester.send_request();
    }

    fn set_content(&self, lines: Lines, output_id: u32) {
        if output_id == self.id {
            let mut buf = self.scrollback.write();
            buf.clear();
            buf.append(lines);
        }
    }

    #[instrument(level = "trace", skip(self))]
    fn append(&self, lines: Lines, outputs: Vec<Output>) {
        let mut primary = false;

        outputs.iter().for_each(|o| {
            if o.id == self.id {
                primary = true;
            }
        });

        if primary {
            let mut completions = self.completions.write();
            let mut current = String::new();
            for line in &lines.full {
                for span in line.iter() {
                    for c in span.content.chars() {
                        if !c.is_alphanumeric() {
                            if !current.is_empty() {
                                completions.insert(std::mem::take(&mut current));
                            }
                        } else {
                            current.push(c);
                        }
                    }
                }
                if !current.is_empty() {
                    let word = std::mem::take(&mut current);
                    completions.insert(word);
                }
            }
            self.scrollback.write().append(lines);
        }
    }

    pub fn receive(&self) -> UnboundedReceiverStream<UiIn> {
        let (tx, rx) = mpsc::unbounded_channel();
        let mut lock = self.input.lock();
        // If there were no receivers previously, flush our buffer to the new
        // one.
        if lock.receivers.is_empty() {
            for msg in lock.buffer.drain(..) {
                let _ = tx.send(msg);
            }
        }
        lock.receivers.push(tx);
        UnboundedReceiverStream::new(rx)
    }
}

fn read_lines(
    state_dir: impl AsRef<Path>,
    shared: Arc<Mutex<TuiInner>>,
    completions: Arc<RwLock<LinkedHashSet<String>>>,
    echo: Arc<AtomicBool>,
) {
    let mut editor = Editor::with_config(
        Config::builder()
            .history_ignore_dups(true)
            .max_history_size(10000)
            .auto_add_history(false)
            .tab_stop(4)
            .bracketed_paste(false)
            .build(),
    );
    let history_file = state_dir.as_ref().join("history.txt");

    if let Err(err) = editor.load_history(&history_file) {
        warn!(?err, "error loading history");
    }
    editor.set_helper(Some(self::input::Helper::new(completions)));
    editor.bind_sequence(
        KeyEvent(KeyCode::Char('R'), Modifiers::ALT),
        EventHandler::Conditional(Box::new(input::SendReload(shared.clone()))),
    );
    editor.bind_sequence(
        KeyEvent(KeyCode::Enter, Modifiers::ALT),
        EventHandler::Conditional(Box::new(input::SendPrevious(shared.clone()))),
    );
    editor.bind_sequence(
        KeyEvent(KeyCode::Up, Modifiers::NONE),
        EventHandler::Conditional(Box::new(input::HintCompleteHandler)),
    );

    thread::spawn(move || {
        while let Ok(line) = editor.readline("") {
            if echo.load(Ordering::Relaxed) {
                editor.add_history_entry(&line);
                if let Err(err) = editor.append_history(&history_file) {
                    warn!(?err, "error writing history file");
                }
            }
            debug!(%line, "rustyline got input line");
            shared_send(&shared, line)
        }
        close_receivers(&shared);
    });
}

#[instrument(level = "trace")]
fn shared_send(inner: &Mutex<TuiInner>, msg: String) {
    inner.lock().send(msg);
}

fn close_receivers(inner: &Mutex<TuiInner>) {
    inner.lock().close_receivers();
}
