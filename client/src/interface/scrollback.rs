use std::{
    cmp,
    sync::Arc,
};

use crossterm::event::{
    Event,
    KeyCode,
    KeyEvent,
    KeyModifiers,
    MouseEvent,
    MouseEventKind,
};
use mudrs_extension::text::Lines;
use mudrs_text::{
    split_words,
    wrap_line,
    CowSpan,
    RingLines,
};
use parking_lot::RwLock;
use tracing::instrument;
use tui::{
    buffer::Buffer,
    layout::Rect,
    widgets::Widget,
};

use super::renderer::{
    NamedRenderRequester,
    RenderRequester,
};

#[derive(Debug, Default, Copy, Clone)]
pub struct Scroll {
    // The bottom line that's visible.
    // May not actually be visible if the offset is high or low enough.
    pub bottom: usize,
    // The visible offset from the the bottom line.
    // A 0 offset means that the bottom line is fully visible, a positive offset
    // means that the line is scrolled up and may be cut off at the bottom if
    // wrapped. A positive offset means that the bottom line isn't actually the
    // bottom. When rendered, it will be updated.
    pub offset: isize,
}

#[derive(Debug)]
pub struct ScrollbackBuffer {
    pub lines: RingLines,
    pub scroll: Option<Scroll>,
    pub my_area: Rect,
    pub mouse: (u16, u16),
    pub last_height: usize,
    pub renderer: NamedRenderRequester,
}

pub type ScrollbackHandle = Arc<RwLock<ScrollbackBuffer>>;

impl ScrollbackBuffer {
    pub fn new(render_requester: RenderRequester) -> Self {
        let renderer = render_requester.with_name("scrollback_buffer");
        let mut buf = ScrollbackBuffer {
            lines: Default::default(),
            scroll: Default::default(),
            last_height: 25,
            renderer,
            my_area: Default::default(),
            mouse: (0, 0),
        };
        // TODO configuration
        buf.lines.set_capacity(5000);
        buf
    }

    #[instrument(level = "trace")]
    pub fn handle_user_event(&mut self, event: &Event) {
        match event {
            Event::Key(KeyEvent {
                code,
                modifiers: KeyModifiers::NONE,
            }) => {
                if !self.has_mouse() {
                    return;
                }
                match code {
                    KeyCode::Enter => self.clear_scroll(),
                    KeyCode::PageDown => self.page_down(),
                    KeyCode::PageUp => self.page_up(),
                    _ => return,
                }
            }
            Event::Mouse(MouseEvent {
                kind, row, column, ..
            }) => {
                if !self.my_mouse(*row, *column) {
                    return;
                }
                match kind {
                    MouseEventKind::ScrollDown => self.scroll(-1),
                    MouseEventKind::ScrollUp => self.scroll(1),
                    _ => return,
                }
            }
            _ => return,
        }
        self.renderer.send_request();
    }

    fn my_mouse(&mut self, row: u16, column: u16) -> bool {
        self.mouse = (row, column);
        self.has_mouse()
    }
    fn has_mouse(&self) -> bool {
        let row_range = self.my_area.top()..self.my_area.bottom();
        let col_range = self.my_area.left()..self.my_area.right();
        row_range.contains(&self.mouse.0) && col_range.contains(&self.mouse.1)
    }

    fn init_scroll(&mut self) -> &mut Scroll {
        self.scroll.get_or_insert(Scroll {
            bottom: self.lines.len(),
            offset: 0,
        })
    }

    pub fn scroll_up_lines(&mut self, by: usize) {
        let scroll = self.init_scroll();
        let bottom = &mut scroll.bottom;
        *bottom = bottom.saturating_sub(by);
    }

    pub fn scroll(&mut self, by: isize) {
        let scroll = self.init_scroll();
        scroll.offset += by;
    }

    pub fn scroll_down_lines(&mut self, by: usize) {
        if let Some(mut scroll) = self.scroll.take() {
            scroll.bottom += by;
            if scroll.bottom < self.lines.len() {
                self.scroll = Some(scroll);
            }
        }
    }

    pub fn page_up(&mut self) {
        self.scroll(self.my_area.height as isize);
    }

    pub fn page_down(&mut self) {
        self.scroll(-(self.my_area.height as isize + 1));
    }

    pub fn clear_scroll(&mut self) {
        self.scroll.take();
    }

    #[instrument(level = "trace", skip_all, fields(buffer = ?self.lines, new = ?lines))]
    pub fn append(&mut self, lines: Lines) {
        let dropped = self.lines.append(lines);
        if self.scroll.is_some() && dropped > 0 {
            self.scroll_up_lines(dropped);
        }
        self.renderer.send_request();
    }

    pub fn clear(&mut self) {
        self.clear_scroll();
        self.lines.clear();
        self.renderer.send_request();
    }
}

impl<'a> Widget for &'a mut ScrollbackBuffer {
    #[instrument(level = "trace", skip_all)]
    fn render(self, area: Rect, buf: &mut Buffer) {
        if area.height == 0 || area.width == 0 {
            return;
        }

        self.my_area = area;

        let (anchor_line, scrolled) = self
            .scroll
            .map(|s| (s.bottom, s.offset))
            .unwrap_or((self.lines.len(), 0));

        // The top line visible.
        // May not actually be due to wrapping.
        let mut start = anchor_line.saturating_sub(area.height as usize);
        let mut end = anchor_line;
        if scrolled < 0 {
            // If scrolled is negative (we're scrolled down), we need to slice further into the buffer.
            end = cmp::min(self.lines.len(), end + (-scrolled as usize));
        }
        if scrolled > 0 {
            // if scrolled is positive (we're scrolled up), we need to slice earlier into the buffer.
            start = start.saturating_sub(scrolled as usize);
        }

        // If we're scrolled, this will likely contain extra data in some
        // direction due to wrapping.
        let lines = self.lines.slice(start..end);

        // Wrapped output lines
        let mut out_lines: Vec<Vec<Vec<CowSpan<'a>>>> = vec![];

        // TODO: Cache this and only recalculate when the screen width changes?
        for line in lines {
            let words = split_words(line);
            let wrapped_lines = wrap_line(&words, area.width as usize)
                .into_iter()
                .map(|wrapped| {
                    wrapped
                        .iter()
                        .flat_map(|word| word.range.spans(line))
                        .collect()
                })
                .collect();
            out_lines.push(wrapped_lines);
        }

        if out_lines.is_empty() {
            return;
        }

        // Calculate the new anchor

        // The index of the anchor in our new view of the lines.
        // Only > 0 if we're scrolled down.
        let anchor_index = (end - start) - (end.saturating_sub(anchor_line));
        let mut new_anchor = anchor_index;
        let mut scroll_tmp = scrolled;
        while scroll_tmp > 0 {
            let current_line = out_lines.get(new_anchor.saturating_sub(1)).unwrap();
            if scroll_tmp >= current_line.len() as isize {
                scroll_tmp -= current_line.len() as isize;
            } else {
                break;
            }

            new_anchor -= 1;

            if new_anchor == 0 {
                break;
            }
        }

        while scroll_tmp < 0 {
            let current_line = out_lines.get(new_anchor.saturating_sub(1)).unwrap();
            if (-scroll_tmp as usize) > current_line.len() {
                scroll_tmp += current_line.len() as isize;
            } else {
                scroll_tmp = out_lines.get(new_anchor).map(|l| l.len()).unwrap_or(1) as isize - 1;
            }
            new_anchor += 1;

            if new_anchor > out_lines.len() {
                break;
            }
        }

        if let Some(mut scroll) = self.scroll.take() {
            scroll.offset = scroll_tmp;
            scroll.bottom =
                (scroll.bottom as isize + (new_anchor as isize - anchor_index as isize)) as usize;
            if scroll.bottom <= self.lines.len() || scroll.offset > 0 {
                self.scroll = Some(scroll);
            }
        }

        // Slice to the new bottom
        let out_lines = &out_lines[..cmp::min(out_lines.len(), new_anchor)];

        let mut last_style = tui::style::Style::default();

        // Display the lines in reverse order
        let mut y = area.height - 1;
        'real: for real in out_lines.iter().rev() {
            for wrapped in real.iter().rev() {
                if scroll_tmp > 0 {
                    scroll_tmp -= 1;
                    continue;
                }

                let mut last_x = None;
                let mut x = 0;
                for g in wrapped.iter().flat_map(|s| s.tui_styled_graphemes()) {
                    if x >= area.width as usize {
                        break;
                    }
                    buf.get_mut(area.x + x as u16, area.y + y)
                        .set_style(g.style)
                        .set_symbol(g.symbol);
                    last_x = Some(x);
                    last_style = g.style;
                    x += unicode_width::UnicodeWidthStr::width(g.symbol);
                }

                let mut fill_style = tui::style::Style::default();

                if let Some(bg) = last_style.bg {
                    fill_style = fill_style.bg(bg);
                }

                // Fill in empty spaces
                if last_x.is_none() || last_x.unwrap() < area.width as usize {
                    let start = last_x.map(|x| x + 1).unwrap_or(0);
                    for x in start..(area.width as usize) {
                        buf.get_mut(area.x + x as u16, area.y + y)
                            .set_style(fill_style);
                    }
                }

                if y == 0 {
                    break 'real;
                } else {
                    y -= 1;
                }
            }
        }
    }
}
