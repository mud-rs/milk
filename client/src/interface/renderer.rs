use std::{
    cmp,
    io,
    sync::{
        atomic::{
            AtomicBool,
            Ordering,
        },
        Arc,
    },
    time::{
        Duration,
        Instant,
    },
};

use crossbeam::channel::{
    unbounded,
    Receiver,
    Sender,
};
use mudrs_extension::text::Lines;
use mudrs_text::Wrap;
use parking_lot::RwLock;
use rustyline::tty::VirtOutput;
#[allow(unused_imports)]
use tracing::debug;
use tracing::{
    instrument,
    trace,
};
use tui::{
    backend::Backend,
    layout::{
        Constraint,
        Direction,
        Layout,
    },
    Terminal,
};

use crate::interface::{
    input::WrappingOutputState,
    scrollback::ScrollbackBuffer,
};

#[derive(Clone, Debug)]
pub struct RenderRequester {
    channel: Sender<(&'static str, Instant)>,
}

impl RenderRequester {
    pub fn with_name(&self, name: &'static str) -> NamedRenderRequester {
        NamedRenderRequester {
            name,
            requester: self.clone(),
        }
    }
}

#[derive(Debug)]
pub struct NamedRenderRequester {
    requester: RenderRequester,
    name: &'static str,
}

impl NamedRenderRequester {
    pub fn send_request(&self) {
        let _ = self.requester.channel.send((self.name, Instant::now()));
    }
}

pub type RenderRequests = Receiver<(&'static str, Instant)>;

pub fn requester() -> (RenderRequester, RenderRequests) {
    let (tx, rx) = unbounded();
    (RenderRequester { channel: tx }, rx)
}

pub struct Renderer<T: Backend + Send> {
    pub terminal: Terminal<T>,

    rl_output: VirtOutput,

    scrollback: Arc<RwLock<ScrollbackBuffer>>,

    echo: Arc<AtomicBool>,

    prompt: Arc<RwLock<Lines>>,
}

impl<T: Backend + Send> Renderer<T> {
    pub fn new(
        mut backend: T,
        rl_output: VirtOutput,
        scrollback: Arc<RwLock<ScrollbackBuffer>>,
        echo: Arc<AtomicBool>,
        prompt: Arc<RwLock<Lines>>,
    ) -> Result<Self, io::Error> {
        backend.clear()?;
        let terminal = Terminal::new(backend)?;
        Ok(Renderer {
            terminal,
            rl_output,
            scrollback,
            prompt,
            echo,
        })
    }

    #[instrument(level = "trace", skip(self))]
    pub fn render(&mut self, full: bool) -> Result<(), anyhow::Error> {
        let terminal = &mut self.terminal;
        let mut scrollback = self.scrollback.write();
        let prompt = self.prompt.read();
        self.rl_output.get_updates()?;
        let input = self.rl_output.state();

        if full {
            terminal.clear()?;
        }

        terminal.draw(|f| {
            let _span = tracing::trace_span!("draw").entered();
            let area = f.size();

            let vertical_chunks = Layout::default()
                .direction(Direction::Horizontal)
                .margin(0)
                .constraints(
                    [
                        Constraint::Percentage(100), // Dead space, tbd
                    ]
                    .as_ref(),
                )
                .split(area);

            let left_chunk = vertical_chunks[0];

            let wrapping = WrappingOutputState(input, self.echo.load(Ordering::Relaxed), &prompt);

            let input_wrapped = wrapping.wrap(left_chunk.width);
            let (cursor_x, cursor_y) = input_wrapped.cursor_pos();
            let input_lines = cmp::max(input_wrapped.lines(), cursor_y);

            trace!(?left_chunk);

            let main_chunks = Layout::default()
                .direction(Direction::Vertical)
                .margin(0)
                .constraints([Constraint::Min(0), Constraint::Length(input_lines)].as_ref())
                .split(left_chunk);

            let input_chunk = main_chunks[1];
            let output_chunk = main_chunks[0];

            trace!(?input_chunk, ?output_chunk);

            f.render_widget(input_wrapped, input_chunk);

            f.render_widget(&mut *scrollback, output_chunk);

            f.set_cursor(input_chunk.x + cursor_x, input_chunk.y + cursor_y);
        })?;
        Ok(())
    }

    pub fn run(self, render_requests: RenderRequests)
    where
        T: Backend + Send + 'static,
    {
        let mut renderer = self;
        std::thread::spawn(move || -> Result<(), anyhow::Error> {
            let max_period = Duration::from_secs_f64(1f64);
            let min_period = Duration::from_secs_f64(1f64 / 60f64);
            renderer.render(true)?;
            let mut new_requests = vec![];
            loop {
                let req = render_requests.recv_timeout(max_period);
                new_requests.clear();
                if let Ok(req) = req {
                    new_requests.push(req);
                    let deadline = Instant::now() + min_period;
                    while let Ok(req) = render_requests.recv_deadline(deadline) {
                        new_requests.push(req);
                    }
                    trace!(requests = ?new_requests, "got render request(s)");
                }
                renderer.render(false)?;
            }
        });
    }
}
