use std::{
    borrow::Cow,
    sync::Arc,
};

use linked_hash_set::LinkedHashSet;
use mudrs_extension::text::{
    Line,
    Lines,
    Spans,
};
use mudrs_text::{
    AnsiProcessor,
    CowSpan,
    Wrap,
};
use parking_lot::{
    Mutex,
    RwLock,
};
use rustyline::{
    completion::Completer,
    highlight::Highlighter,
    hint::{
        Hinter,
        HistoryHinter,
    },
    tty::VirtOutputState,
    validate::{
        MatchingBracketValidator,
        ValidationContext,
        ValidationResult,
        Validator,
    },
    Cmd,
    ConditionalEventHandler,
    Event,
    EventContext,
    KeyCode,
    KeyEvent,
    Modifiers,
    RepeatCount,
};
use tracing::{
    debug,
    instrument,
};
use tui::{
    buffer::Buffer,
    layout::Rect,
    widgets::Widget,
};
use unicode_segmentation::UnicodeSegmentation;

use super::TuiInner;

pub(super) struct SendReload(pub Arc<Mutex<TuiInner>>);
impl ConditionalEventHandler for SendReload {
    fn handle(&self, _: &Event, _: RepeatCount, _: bool, _ctx: &EventContext) -> Option<Cmd> {
        self.0.lock().send_reload();
        Some(Cmd::Noop)
    }
}

pub(super) struct SendPrevious(pub Arc<Mutex<TuiInner>>);
impl ConditionalEventHandler for SendPrevious {
    fn handle(&self, _: &Event, _: RepeatCount, _: bool, _ctx: &EventContext) -> Option<Cmd> {
        self.0.lock().send_prev();
        Some(Cmd::Noop)
    }
}

pub struct HintCompleteHandler;
impl ConditionalEventHandler for HintCompleteHandler {
    fn handle(&self, evt: &Event, _: RepeatCount, _: bool, ctx: &EventContext) -> Option<Cmd> {
        debug_assert_eq!(*evt, Event::from(KeyEvent(KeyCode::Up, Modifiers::NONE)));
        if ctx.has_hint() {
            Some(Cmd::CompleteHint)
        } else {
            None
        }
    }
}

pub struct Helper {
    validator: MatchingBracketValidator,
    hinter: HistoryHinter,
    completions: Arc<RwLock<LinkedHashSet<String>>>,
}

impl Helper {
    pub fn new(completions: Arc<RwLock<LinkedHashSet<String>>>) -> Self {
        Helper {
            validator: Default::default(),
            hinter: HistoryHinter {},
            completions,
        }
    }
}

impl Validator for Helper {
    fn validate(&self, ctx: &mut ValidationContext) -> rustyline::Result<ValidationResult> {
        self.validator.validate(ctx)
    }
    fn validate_while_typing(&self) -> bool {
        self.validator.validate_while_typing()
    }
}

impl Highlighter for Helper {
    fn highlight_char(&self, _line: &str, _pos: usize) -> bool {
        false
    }
    fn highlight_hint<'h>(&self, hint: &'h str) -> std::borrow::Cow<'h, str> {
        Cow::Owned(format!("\x1b[30;1m{hint}\x1b[0m"))
    }
    fn highlight_prompt<'b, 's: 'b, 'p: 'b>(
        &'s self,
        prompt: &'p str,
        default: bool,
    ) -> std::borrow::Cow<'b, str> {
        if !default {
            Cow::Owned(format!("\x1b[32m{prompt}\x1b[0m"))
        } else {
            Cow::Borrowed(prompt)
        }
    }
}

impl Completer for Helper {
    type Candidate = String;
    fn complete(
        &self,
        line: &str,
        pos: usize,
        _ctx: &rustyline::Context<'_>,
    ) -> rustyline::Result<(usize, Vec<Self::Candidate>)> {
        if line.is_empty() || (line.len() != pos && line.as_bytes()[pos] != b' ') {
            return Ok((pos, vec![]));
        }

        let start = line[..pos]
            .rfind(|c: char| !c.is_alphanumeric())
            .map(|p| p + 1)
            .unwrap_or(0);

        let prefix = &line[start..pos];
        let words = self.completions.read();
        let candidates = words
            .iter()
            .rev()
            .filter_map(|w| {
                tracing::trace!(word = %w, "checking candidate");
                if w.starts_with(prefix) {
                    Some(w.clone())
                } else {
                    None
                }
            })
            .collect();

        Ok((start, candidates))
    }
}

impl Hinter for Helper {
    type Hint = <HistoryHinter as Hinter>::Hint;

    fn hint(&self, line: &str, pos: usize, ctx: &rustyline::Context<'_>) -> Option<Self::Hint> {
        self.hinter.hint(line, pos, ctx)
    }
}

impl rustyline::Helper for Helper {}

#[derive(Debug)]
pub struct WrappingOutputState<'a>(pub &'a VirtOutputState, pub bool, pub &'a Lines);

impl<'a> Wrap<'a> for WrappingOutputState<'a> {
    type Wrapped = WrappedInput;

    #[instrument(level = "trace", target = "input::wrap")]
    fn wrap(&'a self, cols: u16) -> Self::Wrapped {
        let (orig_x, orig_y) = (self.0.cursor.col, self.0.cursor.row);
        debug!(target: "input::wrap", orig_x, orig_y, "original cursor pos");

        // Wrap the prompt/status and offset the cursor from rustyline
        let mut text = self.2.wrap(cols);
        let orig_y = orig_y + text.full.len();
        let orig_x = orig_x
            + text
                .r#final
                .as_ref()
                .map(|s| {
                    s.iter()
                        .fold(0, |l, s| l + s.content.graphemes(true).count())
                })
                .unwrap_or_default();
        debug!(target: "input::wrap", ?text, "initial input buffer");

        // Process ANSI and add it to the buffer to render
        let mut processor = AnsiProcessor::new();
        processor.append(&self.0.buffer);
        text.append(processor.take());
        debug!(target: "input::wrap", ?text, "processed rustyline input state");

        // Scan through the buffer and figure out the wrapped cursor position.
        let mut final_lines = 0;
        let (mut final_x, mut final_y) = (0, 0);

        let lines = text
            .full
            .iter()
            .map(|Line(l)| l)
            .chain(text.r#final.as_ref().map(|Spans(s)| s));
        debug!(target: "input::wrap", x = orig_x, y = orig_y, "adjusted cursor pos for prompt");

        for (current_y, line) in lines.enumerate() {
            let len = line
                .iter()
                .fold(0, |l, s| l + s.content.graphemes(true).count());

            // On the last cursor line, set x and only wrap y by the x position.
            if current_y == orig_y {
                if orig_x >= cols as usize {
                    final_y += orig_x / cols as usize;
                }
                final_x = orig_x % cols as usize;
            }
            // On every line before the final line, also increment the final y
            // and additionally wrap it by the total line length.
            if current_y < orig_y {
                final_y += 1;
            }

            if len > cols as usize {
                final_lines += len / cols as usize;
            }
            // These get updated unconditionally
            final_lines += 1;
        }

        debug!(target: "input::wrap", final_x, final_y, final_lines, "final cursor pos and lines");

        WrappedInput {
            text,
            cursor: (final_x as u16, final_y as u16),
            lines: final_lines as u16,
            noecho: !self.1,
        }
    }
}

#[derive(Debug)]
pub struct WrappedInput {
    text: Lines,
    cursor: (u16, u16),
    lines: u16,
    noecho: bool,
}

impl WrappedInput {
    pub fn lines(&self) -> u16 {
        self.lines
    }
    pub fn cursor_pos(&self) -> (u16, u16) {
        self.cursor
    }
}

impl Widget for WrappedInput {
    fn render(self, area: Rect, buf: &mut Buffer) {
        let mut y = 0;
        let lines = self
            .text
            .full
            .iter()
            .map(|Line(l)| l)
            .chain(self.text.r#final.as_ref().map(|Spans(s)| s));
        for line in lines {
            if area.y + y as u16 >= area.bottom() {
                break;
            }
            let mut x = 0;
            for span in line.iter() {
                for g in CowSpan::from(span).tui_styled_graphemes() {
                    if area.x + x as u16 >= area.right() {
                        x = 0;
                        y += 1;
                    }
                    if area.y + y as u16 >= area.bottom() {
                        break;
                    }
                    let symbol = if self.noecho { "*" } else { g.symbol };
                    buf.get_mut(area.x + x as u16, area.y + y as u16)
                        .set_symbol(symbol)
                        .set_style(g.style);
                    x += 1;
                }
            }
            y += 1;
        }
    }
}
