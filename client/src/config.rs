use std::{
    borrow::{
        Borrow,
        Cow,
    },
    collections::HashMap,
    hash::Hash,
    path::{
        Path,
        PathBuf,
    },
};

use clap::Parser;
use serde::{
    Deserialize,
    Serialize,
};
use url::Url;

#[derive(Debug, Serialize, Deserialize, Clone, Default, PartialEq, Eq)]
#[serde(default)]
pub struct Config {
    #[serde(flatten)]
    pub profiles: HashMap<String, Profile>,
}

#[derive(Parser, Debug, Serialize, Deserialize, Default, Clone, PartialEq, Eq)]
#[serde(default)]
pub struct Profile {
    /// Enable scripting or override the default.
    #[clap(long, value_parser)]
    pub script: Option<PathBuf>,
    /// Enable the remote connector or override the default.
    #[clap(long, value_parser)]
    pub remote: Option<String>,
    /// Override the UI ID [default: 0]
    #[clap(long, value_parser)]
    pub ui: Option<u32>,
    /// Enable the listener or override the default.
    #[clap(long, value_parser)]
    pub listen: Option<Url>,
    /// Connect to another milk instance. Implies --no-listen.
    #[clap(long, value_parser)]
    pub connect: Option<Url>,
    /// State directory.
    /// Holds things like logs or the unix socket.
    #[clap(long, value_parser)]
    pub state: Option<PathBuf>,
}

fn expand_path(p: impl AsRef<Path>) -> PathBuf {
    let path_string = format!("{}", p.as_ref().display());
    let expanded = shellexpand::env(&path_string).unwrap_or(Cow::Borrowed(&path_string));
    expanded.into_owned().into()
}

impl Profile {
    pub fn expand_env(self) -> Profile {
        Profile {
            listen: self.listen,
            connect: self.connect,
            remote: self.remote,
            ui: self.ui,
            script: self.script.map(expand_path),
            state: self.state.map(expand_path),
        }
    }

    pub fn with_overrides(self, other: Profile) -> Profile {
        Profile {
            remote: other.remote.or(self.remote),
            ui: other.ui.or(self.ui),
            script: other.script.or(self.script),
            listen: other.listen.or(self.listen),
            connect: other.connect.or(self.connect),
            state: other.state.or(self.state),
        }
    }
}

impl Config {
    pub fn get_profile<S>(&self, name: &S) -> Option<Profile>
    where
        String: Borrow<S>,
        S: Hash + Eq + ?Sized,
    {
        self.profiles.get(name).cloned()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn default_config() {
        let config = Config::default();

        let serialized = toml::to_string_pretty(&config).expect("serialized config");

        println!("serialized:\n{serialized}");

        let deserialized = toml::from_str(&serialized).expect("deserialized config");

        assert_eq!(config, deserialized);
    }

    #[test]
    fn test_profiles() {
        const TEST_INPUT: &str = r#"
[default]
script = "/path/to/script.rn"
remote = "localhost:1234"
"#;

        let deserialized: Config = toml::from_str(TEST_INPUT).expect("deserialized");

        assert_eq!(
            deserialized,
            Config {
                profiles: vec![(
                    "default".to_string(),
                    Profile {
                        script: Some("/path/to/script.rn".into()),
                        remote: Some("localhost:1234".into()),
                        ..Default::default()
                    }
                )]
                .into_iter()
                .collect(),
            }
        );

        let somemud = deserialized.get_profile("default").expect("profile");
        assert_eq!(somemud.ui, None);
    }
}
