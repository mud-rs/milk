use std::path::PathBuf;

use anyhow::Error;
use async_compression::tokio::write::GzipEncoder;
use chrono::Local;
use futures::prelude::*;
use mudrs_extension::bus::{
    Bus,
    Event,
};
use serde_json::json;
use tokio::io::AsyncWriteExt;
use tracing::error;

pub struct Logger {
    pub path: PathBuf,
}

impl Logger {
    pub async fn start(self, bus: impl Bus) -> Result<(), Error> {
        let mut client = bus.get_client("logger").await?;
        client.subscribe(mudrs_extension::bus::Topic::UiOut).await?;
        client.subscribe(mudrs_extension::bus::Topic::UiIn).await?;
        client
            .subscribe(mudrs_extension::bus::Topic::RemoteIn)
            .await?;
        client
            .subscribe(mudrs_extension::bus::Topic::RemoteOut)
            .await?;

        let path = format!("{}.gz", self.path.display());
        let mut output = GzipEncoder::new(tokio::fs::File::create(&path).await?);

        tokio::spawn(
            async move {
                let mut events = client.events().unwrap();

                while let Some(Event::Publish(event)) = events.try_next().await? {
                    let ev = format!(
                        "{}\n",
                        serde_json::to_string(&json!({
                            "timestamp": Local::now(),
                            "topic": event.topic(),
                            "msg": &event,
                        }))?
                    );

                    output.write_all(ev.as_bytes()).await?;
                    output.flush().await?;
                }

                Ok::<(), Error>(())
            }
            .and_then(|_| async move {
                error!("logger exited");
                Ok(())
            })
            .or_else(|err| async move {
                error!(?err, "logger exited");
                Err(err)
            }),
        );

        Ok(())
    }
}
