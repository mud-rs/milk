Note: this is *very* WIP and does not accurately reflect much of anything.

# Mud-rs Extension API
Service definitions to allow users to build their own interfaces, scripting
engines, and remote connectors.

## Overview
At its core, the extension API functions as an event bus. Components produce
events to communicate with other components, and consume events to decide what
actions to take. There are two major types of event:

* *Remote*: Events to or from the upstream MUD server. These can be displayable
  lines of text, GMCP or MSDP messages, or other telnet control sequences. They
  can also be messages to be sent *to* the upstream server.
* *Interface*: UI events. These can be typed commands or bound keys from the
  user, or lines of text intended for display.

## Components
Anything that consumes or produces events is a "component." This includes the
connection to the MUD server and the UI, as well as the "script engine" that
drives everything. Since all components have access to the same events, the
only distinction between them is what types of events they produce and respond
to. That said, they will generally fall into three major categories:

* *Remote*: Producers of "from" remote events, and consumers of "to" remote
  events.
* *Interface*: Producers of "from" UI events, and consumers of "to" UI events.
* *Script*: The glue that ties the Interface and Remote components together.
  Consumes "from" UI and Remote events and produces "to" UI and Remote events.

It is possible for a component to not strictly fall into one of these
categories. For example, a "logger" component may consume **all** event types
to accurately record everything that occurred.

## The Bus
The "event bus" provides the interface by which [components](#components)
exchange messages.

--------------
# WIP: Everything below this point is *very* old and definitely inaccurate.

## Service Types
### Remote
The Remote service manages the connection to the remote server. This may be a
direct telnet connection, a connection through a bouncer, or local/mocked. Most
protocol-level details of the connection are expected to be handled by the
component providing the Remote service. Exceptions to this are telnet EOR/GA,
MSDP, and GCMP messages.

Once a component providing the Remote service connects, messages will be
pulled from it via the Receive method. Other components will send messages via
its Send method.

There may only be one Remote service at a time.

### Interface
The Interface service represents what the user directly interacts with. This may
be a scrollback buffer and text input box, a text-to-speech system, or mocked.
At this time, only simple output lines, prompt, and input commands are
supported. More advanced UI elements may be added in the future.

As with the Remote service, once a component implmenting the Interface service
connects, input events will be pulled via its Receive method.

There may be multiple active Interface components. Text to be displayed to the
user will be fanned out to all components that provide Interface.

### Script
The Script service is "special" in that it has no methods of its own. Instead,
if there is no component connected that declares that it provides the Script
service, the Remote and Interface streams will be handled differently. User
input will be routed directly to the component providing the Remote service and
also looped back to the Interface component. Messages from the Remote will be
routed directly to the Interface.

If there *is* a component registered that declares that it provides the Script
service, Remote and Interface messages will only be routed to other components
that call their Receive methods. Note that a component can still receive these
messages *without* being registered as a Script provider. This is useful for
components that provide logging facilities.

## The Core
The "Core" is the main process. It's responsible for providing the mechanism
that components use to provide their services or call other services. It acts as
a service router to make sure that requests from one component reach the proper
destination.