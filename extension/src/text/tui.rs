//! Compatibility between mudrs_extension::text and tui types.

use std::borrow::Cow;

mod ext {
    pub use tui::{
        style::*,
        text::Span,
    };
}

use super::*;

impl TryFrom<ext::Color> for Color {
    type Error = ();

    fn try_from(tui_color: ext::Color) -> Result<Self, Self::Error> {
        Ok(match tui_color {
            ext::Color::Reset => return Err(()),
            ext::Color::Black => Color::Black,
            ext::Color::Red => Color::Red,
            ext::Color::Green => Color::Green,
            ext::Color::Yellow => Color::Yellow,
            ext::Color::Blue => Color::Blue,
            ext::Color::Magenta => Color::Magenta,
            ext::Color::Cyan => Color::Cyan,
            ext::Color::Gray => Color::White,
            ext::Color::DarkGray => Color::BrightBlack,
            ext::Color::LightRed => Color::BrightRed,
            ext::Color::LightGreen => Color::BrightGreen,
            ext::Color::LightYellow => Color::BrightYellow,
            ext::Color::LightBlue => Color::BrightBlue,
            ext::Color::LightMagenta => Color::BrightMagenta,
            ext::Color::LightCyan => Color::BrightCyan,
            ext::Color::White => Color::BrightWhite,
            ext::Color::Rgb(r, g, b) => {
                return Ok(Color::Rgb(r, g, b));
            }
            ext::Color::Indexed(i) => {
                return Ok(Color::Indexed(i));
            }
        })
    }
}

impl From<ext::Style> for Style {
    fn from(tui_style: ext::Style) -> Self {
        let tui_mods = tui_style.add_modifier;
        Style {
            fg: tui_style.fg.map(Color::try_from).and_then(Result::ok),
            bg: tui_style.bg.map(Color::try_from).and_then(Result::ok),
            bold: tui_mods.contains(ext::Modifier::BOLD),
            dim: tui_mods.contains(ext::Modifier::DIM),
            italic: tui_mods.contains(ext::Modifier::ITALIC),
            underlined: tui_mods.contains(ext::Modifier::UNDERLINED),
            slow_blink: tui_mods.contains(ext::Modifier::SLOW_BLINK),
            rapid_blink: tui_mods.contains(ext::Modifier::RAPID_BLINK),
            reversed: tui_mods.contains(ext::Modifier::REVERSED),
            hidden: tui_mods.contains(ext::Modifier::HIDDEN),
            crossed_out: tui_mods.contains(ext::Modifier::CROSSED_OUT),
        }
    }
}

impl From<Color> for ext::Color {
    fn from(col: Color) -> Self {
        match col {
            Color::Black => ext::Color::Black,
            Color::Red => ext::Color::Red,
            Color::Green => ext::Color::Green,
            Color::Yellow => ext::Color::Yellow,
            Color::Blue => ext::Color::Blue,
            Color::Magenta => ext::Color::Magenta,
            Color::Cyan => ext::Color::Cyan,
            Color::White => ext::Color::Gray,
            Color::BrightBlack => ext::Color::DarkGray,
            Color::BrightRed => ext::Color::LightRed,
            Color::BrightGreen => ext::Color::LightGreen,
            Color::BrightYellow => ext::Color::LightYellow,
            Color::BrightBlue => ext::Color::LightBlue,
            Color::BrightMagenta => ext::Color::LightMagenta,
            Color::BrightCyan => ext::Color::LightCyan,
            Color::BrightWhite => ext::Color::White,
            Color::Indexed(i) => ext::Color::Indexed(i),
            Color::Rgb(r, g, b) => ext::Color::Rgb(r, g, b),
        }
    }
}

impl From<Style> for ext::Style {
    fn from(other: Style) -> Self {
        let mut tui_mods = ext::Modifier::empty();

        if other.bold {
            tui_mods.insert(ext::Modifier::BOLD)
        }
        if other.dim {
            tui_mods.insert(ext::Modifier::DIM)
        }
        if other.italic {
            tui_mods.insert(ext::Modifier::ITALIC)
        }
        if other.underlined {
            tui_mods.insert(ext::Modifier::UNDERLINED)
        }
        if other.slow_blink {
            tui_mods.insert(ext::Modifier::SLOW_BLINK)
        }
        if other.rapid_blink {
            tui_mods.insert(ext::Modifier::RAPID_BLINK)
        }
        if other.reversed {
            tui_mods.insert(ext::Modifier::REVERSED)
        }
        if other.hidden {
            tui_mods.insert(ext::Modifier::HIDDEN)
        }
        if other.crossed_out {
            tui_mods.insert(ext::Modifier::CROSSED_OUT)
        }

        ext::Style::reset().patch(ext::Style {
            fg: other.fg.map(From::from).unwrap_or(ext::Color::Reset).into(),
            bg: other.bg.map(From::from).unwrap_or(ext::Color::Reset).into(),

            add_modifier: tui_mods,
            ..Default::default()
        })
    }
}

impl From<ext::Span<'_>> for Span {
    fn from(span: ext::Span<'_>) -> Self {
        Span {
            content: span.content.into_owned(),
            style: span.style.into(),
        }
    }
}

impl From<Span> for ext::Span<'_> {
    fn from(span: Span) -> Self {
        ext::Span {
            content: Cow::Owned(span.content),
            style: span.style.into(),
        }
    }
}
