use std::ops::{
    Deref,
    DerefMut,
};

use rune::{
    termcolor,
    Any,
};
use serde::{
    Deserialize,
    Serialize,
};
use termcolor::Color as TColor;

mod tui;

#[derive(Debug, Clone, Default, Serialize, Deserialize, Any)]
pub struct Lines {
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub full: Vec<Line>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub r#final: Option<Spans>,
}

impl Lines {
    pub fn complete_final(&mut self) {
        if let Some(Spans(l)) = self.r#final.take() {
            self.push_line(Line(l));
        }
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize, Eq, PartialEq, Any)]
pub struct Line(pub Vec<Span>);
#[derive(Debug, Clone, Default, Serialize, Deserialize, Eq, PartialEq, Any)]
pub struct Spans(pub Vec<Span>);

impl Deref for Spans {
    type Target = Vec<Span>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Spans {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl Deref for Line {
    type Target = Vec<Span>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Line {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

#[derive(Debug, Clone, Default, Serialize, Deserialize, Eq, PartialEq, Any)]
pub struct Span {
    #[rune(get, set)]
    #[serde(default)]
    pub content: String,
    #[rune(get, set)]
    #[serde(default)]
    pub style: Style,
}

fn is_false(v: &bool) -> bool {
    !v
}

#[derive(Debug, Copy, Clone, Default, Serialize, Deserialize, Eq, PartialEq, Any)]
pub struct Style {
    #[serde(default, skip_serializing_if = "Option::is_none")]
    #[rune(get, set)]
    pub fg: Option<Color>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    #[rune(get, set)]
    pub bg: Option<Color>,

    #[serde(default, skip_serializing_if = "is_false")]
    #[rune(get, set)]
    pub bold: bool,
    #[serde(default, skip_serializing_if = "is_false")]
    #[rune(get, set)]
    pub dim: bool,
    #[serde(default, skip_serializing_if = "is_false")]
    #[rune(get, set)]
    pub italic: bool,
    #[serde(default, skip_serializing_if = "is_false")]
    #[rune(get, set)]
    pub underlined: bool,
    #[serde(default, skip_serializing_if = "is_false")]
    #[rune(get, set)]
    pub slow_blink: bool,
    #[serde(default, skip_serializing_if = "is_false")]
    #[rune(get, set)]
    pub rapid_blink: bool,
    #[serde(default, skip_serializing_if = "is_false")]
    #[rune(get, set)]
    pub reversed: bool,
    #[serde(default, skip_serializing_if = "is_false")]
    #[rune(get, set)]
    pub hidden: bool,
    #[serde(default, skip_serializing_if = "is_false")]
    #[rune(get, set)]
    pub crossed_out: bool,
}

#[derive(Any, Clone, Copy, Debug, Serialize, Deserialize, Eq, PartialEq)]
pub enum Color {
    #[rune(constructor)]
    Black,
    #[rune(constructor)]
    Red,
    #[rune(constructor)]
    Green,
    #[rune(constructor)]
    Yellow,
    #[rune(constructor)]
    Blue,
    #[rune(constructor)]
    Magenta,
    #[rune(constructor)]
    Cyan,
    #[rune(constructor)]
    White,
    #[rune(constructor)]
    BrightBlack,
    #[rune(constructor)]
    BrightRed,
    #[rune(constructor)]
    BrightGreen,
    #[rune(constructor)]
    BrightYellow,
    #[rune(constructor)]
    BrightBlue,
    #[rune(constructor)]
    BrightMagenta,
    #[rune(constructor)]
    BrightCyan,
    #[rune(constructor)]
    BrightWhite,
    #[rune(constructor)]
    Indexed(#[rune(get, set)] u8),
    Rgb(
        #[rune(get, set)] u8,
        #[rune(get, set)] u8,
        #[rune(get, set)] u8,
    ),
}

impl Color {
    pub fn intense(self, intense: bool) -> Color {
        if intense {
            match self {
                Color::Black => Color::BrightBlack,
                Color::Red => Color::BrightRed,
                Color::Green => Color::BrightGreen,
                Color::Yellow => Color::BrightYellow,
                Color::Blue => Color::BrightBlue,
                Color::Magenta => Color::BrightMagenta,
                Color::Cyan => Color::BrightCyan,
                Color::White => Color::BrightWhite,
                other => other,
            }
        } else {
            match self {
                Color::BrightBlack => Color::Black,
                Color::BrightRed => Color::Red,
                Color::BrightGreen => Color::Green,
                Color::BrightYellow => Color::Yellow,
                Color::BrightBlue => Color::Blue,
                Color::BrightMagenta => Color::Magenta,
                Color::BrightCyan => Color::Cyan,
                Color::BrightWhite => Color::White,
                other => other,
            }
        }
    }
}

impl Style {
    pub fn bold(self, bold: bool) -> Self {
        Self { bold, ..self }
    }
    pub fn bg(self, color: Color) -> Self {
        Self {
            bg: Some(color),
            ..self
        }
    }
    pub fn fg(self, color: Color) -> Self {
        Self {
            fg: Some(color),
            ..self
        }
    }
}
impl Lines {
    pub fn new(content: impl AsRef<str>, style: Style) -> Self {
        let mut this = Lines::default();
        let lines = content
            .as_ref()
            .split('\n')
            .map(|s| s.trim_matches('\r'))
            .map(|s| {
                (
                    s.is_empty(),
                    Line(vec![Span {
                        content: s.into(),
                        style,
                    }]),
                )
            });

        let mut last_empty = true;
        for (empty, line) in lines {
            this.push_line(line);
            last_empty = empty;
        }

        // If the last line wasn't empty, add it as the last incomplete line.
        if !last_empty {
            this.r#final = this.full.pop().map(|Line(l)| Spans(l));
        } else {
            // Otherwise, get rid of it altogether.
            let _ = this.full.pop();
        }
        this
    }

    pub fn push_line(&mut self, Line(mut line): Line) {
        self.full
            .push(if let Some(Spans(mut completed)) = self.r#final.take() {
                completed.append(&mut line);
                Line(completed)
            } else {
                Line(line)
            })
    }

    pub fn push_spans(&mut self, Spans(mut spans): Spans) {
        let Spans(last) = self.r#final.get_or_insert_with(Default::default);
        last.append(&mut spans);
    }

    pub fn append(&mut self, mut lines: Lines) {
        lines.full.drain(..).for_each(|l| self.push_line(l));
        if let Some(l) = lines.r#final.take() {
            self.push_spans(l)
        }
    }
}

impl TryFrom<TColor> for Color {
    type Error = ();
    fn try_from(color: TColor) -> Result<Color, ()> {
        Ok(match color {
            TColor::Black => Color::Black,
            TColor::Blue => Color::Blue,
            TColor::Green => Color::Green,
            TColor::Red => Color::Red,
            TColor::Cyan => Color::Cyan,
            TColor::Magenta => Color::Magenta,
            TColor::Yellow => Color::Yellow,
            TColor::White => Color::White,
            TColor::Ansi256(u) => Color::Indexed(u),
            TColor::Rgb(r, g, b) => Color::Rgb(r, g, b),
            _ => return Err(()),
        })
    }
}
