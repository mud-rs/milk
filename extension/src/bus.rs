use std::{
    collections::HashMap,
    fmt,
    pin::Pin,
    sync::Arc,
    task::{
        Context,
        Poll,
    },
};

use anyhow::Error;
use async_trait::async_trait;
use busrt::{
    borrow::Cow,
    broker::{
        self,
        ClientAaa,
        ServerConfig,
    },
    client,
    ipc,
    EventChannel,
    Frame,
    FrameKind,
    QoS,
};
use futures::{
    prelude::*,
    ready,
    stream::FusedStream,
};
use rune::Any;
use serde::{
    Deserialize,
    Serialize,
};
use tokio::sync::Mutex;
use tracing::{
    debug,
    instrument,
};
use url::Url;

use crate::{
    interface::{
        Mode,
        Output,
        UiIn,
        UiOut,
    },
    remote::{
        RemoteIn,
        RemoteOut,
    },
    text::Lines,
};

#[derive(Any)]
pub struct InternalBus {
    broker: broker::Broker,
    url: Option<Url>,
}

impl Default for InternalBus {
    fn default() -> Self {
        InternalBus::new()
    }
}

impl InternalBus {
    pub fn new() -> Self {
        InternalBus {
            broker: broker::Broker::new(),
            url: None,
        }
    }

    pub async fn listen(
        &mut self,
        url: Url,
        aaa: impl Into<Option<HashMap<String, ClientAaa>>>,
    ) -> Result<(), Error> {
        let addr = url_to_addr(&url)?;
        match url.scheme() {
            #[cfg(not(target_os = "windows"))]
            "unix" => {
                debug!("listening for unix connections");
                self.listen_unix(&addr, aaa).await?;
            }
            "tcp" => {
                debug!("listening for tcp connections");
                self.listen_tcp(&addr, aaa).await?;
            }
            _ => unreachable!(),
        }
        self.url = Some(url);
        Ok(())
    }

    #[instrument(skip(self, aaa))]
    pub async fn listen_tcp(
        &mut self,
        addr: &str,
        aaa: impl Into<Option<HashMap<String, ClientAaa>>>,
    ) -> Result<(), Error> {
        Ok(self
            .broker
            .spawn_tcp_server(addr, server_config(aaa))
            .await
            .map_err(BusError)?)
    }

    #[cfg(not(target_os = "windows"))]
    pub async fn listen_unix(
        &mut self,
        path: &str,
        aaa: impl Into<Option<HashMap<String, ClientAaa>>>,
    ) -> Result<(), Error> {
        Ok(self
            .broker
            .spawn_unix_server(path, server_config(aaa))
            .await
            .map_err(BusError)?)
    }

    pub async fn register_client(&self, name: &str) -> Result<Client, Error> {
        Ok(Client {
            client: Box::new(self.broker.register_client(name).await.map_err(BusError)?),
        })
    }
}

fn server_config(aaa: impl Into<Option<HashMap<String, ClientAaa>>>) -> ServerConfig {
    let mut config = broker::ServerConfig::default();
    if let Some(aaa) = aaa.into() {
        config = config.aaa_map(Arc::new(parking_lot::Mutex::new(aaa)));
    }
    config
}

#[derive(Any)]
pub struct Client {
    client: Box<dyn client::AsyncClient>,
}

#[derive(Debug)]
struct BusError(busrt::Error);

impl fmt::Display for BusError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl std::error::Error for BusError {}

impl Client {
    pub async fn connect(addr: &str, name: &str) -> Result<Self, Error> {
        let config = ipc::Config::new(addr, name);
        Ok(Client {
            client: Box::new(ipc::Client::connect(&config).await.map_err(BusError)?),
        })
    }

    #[instrument(level = "trace", skip(self))]
    pub async fn publish(&mut self, event: PublishEvent) -> Result<(), Error> {
        let buf = serde_json::to_vec(&event)?;
        let resp = self
            .client
            .publish(event.topic(), Cow::Owned(buf), QoS::RealtimeProcessed)
            .await
            .map_err(BusError)?
            .unwrap();

        Ok(resp.await?.map_err(BusError)?)
    }

    pub fn events(&mut self) -> Option<Events> {
        self.client
            .take_event_channel()
            .map(|channel| Events { channel })
    }

    pub async fn subscribe(&mut self, topic: Topic) -> Result<(), Error> {
        let resp = self
            .client
            .subscribe(topic.name(), QoS::RealtimeProcessed)
            .await
            .map_err(BusError)?
            .unwrap();
        Ok(resp.await?.map_err(BusError)?)
    }

    pub async fn subscribe_bulk(&mut self, topics: &[Topic]) -> Result<(), Error> {
        let topics = topics.iter().map(|t| t.name()).collect::<Vec<_>>();
        let resp = self
            .client
            .subscribe_bulk(topics.as_slice(), QoS::RealtimeProcessed)
            .await
            .map_err(BusError)?
            .unwrap();
        Ok(resp.await?.map_err(BusError)?)
    }
}

#[derive(Debug, Copy, Clone, Any)]
pub enum Topic {
    // Messages from the MUD server
    RemoteIn,
    // Messages to be sent to the MUD server
    RemoteOut,
    // Commands, keys, etc. from the User
    UiIn,
    // Things to display to the User
    UiOut,
}

impl Topic {
    fn name(self) -> &'static str {
        use Topic::*;
        match self {
            RemoteIn => "remote.in",
            RemoteOut => "remote.out",
            UiIn => "ui.in",
            UiOut => "ui.out",
        }
    }
}

impl<'a> TryFrom<&'a str> for Topic {
    type Error = String;

    fn try_from(other: &'a str) -> Result<Self, Self::Error> {
        use Topic::*;
        Ok(match other {
            "remote.in" => RemoteIn,
            "remote.out" => RemoteOut,
            "ui.in" => UiIn,
            "ui.out" => UiOut,
            other => return Err(String::from(other)),
        })
    }
}

#[derive(Any)]
pub struct Events {
    channel: EventChannel,
}

impl Stream for Events {
    type Item = Result<Event, Error>;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let frame = ready!(self.channel.poll_next_unpin(cx));
        Poll::Ready(frame.map(Event::try_from))
    }
}

impl FusedStream for Events {
    fn is_terminated(&self) -> bool {
        self.channel.is_terminated()
    }
}

impl Events {
    pub async fn recv(&mut self) -> Result<Option<Event>, Error> {
        self.next().await.transpose()
    }
}

#[derive(Debug, Any, Clone)]
pub enum Event {
    Publish(PublishEvent),
}

impl TryFrom<Frame> for Event {
    type Error = Error;
    fn try_from(frame: Frame) -> Result<Self, Self::Error> {
        debug!(?frame, "got frame");
        Ok(match frame.kind() {
            FrameKind::Publish => Self::Publish(PublishEvent::try_from(frame)?),
            other => anyhow::bail!("unhandle frame kind: {:?}", other),
        })
    }
}

#[derive(Debug, Any, Clone, Serialize, Deserialize)]
pub enum PublishEvent {
    RemoteIn(RemoteIn),
    RemoteOut(RemoteOut),
    UiIn(UiIn),
    UiOut(UiOut),
}

impl PublishEvent {
    pub fn topic(&self) -> &'static str {
        match self {
            PublishEvent::RemoteIn(_) => "remote.in",
            PublishEvent::RemoteOut(_) => "remote.out",
            PublishEvent::UiIn(_) => "ui.in",
            PublishEvent::UiOut(_) => "ui.out",
        }
    }
}

impl TryFrom<Frame> for PublishEvent {
    type Error = Error;
    fn try_from(other: Frame) -> Result<Self, Self::Error> {
        Ok(match other.topic() {
            Some(_topic) => serde_json::from_slice(other.payload())?,
            None => anyhow::bail!("no topic found"),
        })
    }
}

#[async_trait]
pub trait Bus {
    async fn get_client(&self, name: &str) -> Result<Client, Error>;
    fn get_url(&self) -> Option<&Url>;
}

#[async_trait]
impl Bus for InternalBus {
    async fn get_client(&self, name: &str) -> Result<Client, Error> {
        self.register_client(name).await
    }
    fn get_url(&self) -> Option<&Url> {
        self.url.as_ref()
    }
}

pub struct ExternalBus {
    addr: Url,
}

impl ExternalBus {
    pub fn new(addr: Url) -> Result<Self, Error> {
        url_to_addr(&addr)?;
        Ok(ExternalBus { addr })
    }
    async fn connect(&self, name: &str) -> Result<Client, Error> {
        let config = ipc::Config::new(url_to_addr(&self.addr)?.as_str(), name);
        Ok(Client {
            client: Box::new(ipc::Client::connect(&config).await.map_err(BusError)?),
        })
    }
}

#[async_trait]
impl Bus for ExternalBus {
    async fn get_client(&self, name: &str) -> Result<Client, Error> {
        self.connect(name).await
    }
    fn get_url(&self) -> Option<&Url> {
        Some(&self.addr)
    }
}

pub enum AnyBus {
    Internal(InternalBus),
    External(ExternalBus),
}

impl From<InternalBus> for AnyBus {
    fn from(bus: InternalBus) -> Self {
        AnyBus::Internal(bus)
    }
}

impl From<ExternalBus> for AnyBus {
    fn from(bus: ExternalBus) -> Self {
        AnyBus::External(bus)
    }
}

#[async_trait]
impl Bus for AnyBus {
    async fn get_client(&self, name: &str) -> Result<Client, Error> {
        match self {
            AnyBus::Internal(bus) => bus.get_client(name).await,
            AnyBus::External(bus) => bus.get_client(name).await,
        }
    }
    fn get_url(&self) -> Option<&Url> {
        match self {
            AnyBus::Internal(bus) => bus.get_url(),
            AnyBus::External(bus) => bus.get_url(),
        }
    }
}

#[async_trait]
impl<B> Bus for Arc<B>
where
    B: Bus + Send + Sync,
{
    async fn get_client(&self, name: &str) -> Result<Client, Error> {
        B::get_client(self, name).await
    }
    fn get_url(&self) -> Option<&Url> {
        B::get_url(self)
    }
}

fn url_to_addr(url: &Url) -> Result<String, Error> {
    Ok(match url.scheme() {
        "unix" => {
            let mut path: String = url.path().into();
            if let Some(host) = url.host() {
                path = format!("/{host}{path}");
            }
            path
        }
        "tcp" => format!(
            "{}:{}",
            url.host().expect("hostname"),
            url.port().unwrap_or(2517)
        ),
        scheme => anyhow::bail!("unrecognized scheme: {}", scheme),
    })
}

#[derive(Any, Clone)]
pub struct SharedClient(pub Arc<Mutex<Client>>);

impl SharedClient {
    pub fn new(client: Client) -> Self {
        SharedClient(Arc::new(Mutex::new(client)))
    }
    #[instrument(level = "trace", skip(self))]
    pub async fn publish(&self, msg: PublishEvent) -> Result<(), Error> {
        self.0.lock().await.publish(msg).await
    }
    #[instrument(level = "trace", skip(self))]
    pub async fn send(&self, cmd: String) -> rune::Result<()> {
        self.publish(PublishEvent::RemoteOut(RemoteOut::Command(cmd)))
            .await
    }
    #[instrument(level = "trace", skip(self))]
    pub async fn set_prompt(&mut self, output_id: u32, lines: Lines) -> rune::Result<()> {
        self.publish(PublishEvent::UiOut(UiOut::SetPrompt { lines, output_id }))
            .await
    }
    #[instrument(level = "trace", skip(self))]
    pub async fn append(&self, outputs: &[(u32, Mode)], lines: Lines) -> rune::Result<()> {
        let outputs = outputs
            .iter()
            .cloned()
            .map(|(id, mode)| Output { id, mode })
            .collect();
        self.publish(PublishEvent::UiOut(UiOut::Append { lines, outputs }))
            .await
    }

    pub async fn set_content(&self, output_id: u32, lines: Lines) -> rune::Result<()> {
        self.publish(PublishEvent::UiOut(UiOut::SetContent { lines, output_id }))
            .await
    }

    #[instrument(level = "trace", skip(self))]
    pub async fn set_echo(&self, echo: bool) -> rune::Result<()> {
        self.publish(PublishEvent::UiOut(UiOut::SetEcho(echo)))
            .await
    }
}
