use std::collections::HashMap;

use rune::Any;
use serde::{
    Deserialize,
    Serialize,
};

use crate::text::Lines;

#[derive(Clone, Debug, Serialize, Deserialize, Any)]
pub enum RemoteIn {
    Lines(Lines),
    Gmcp {
        command: String,
        data: Vec<u8>,
    },
    Msdp {
        name: String,
        values: Vec<MsdpValue>,
    },
    Echo(bool),
	Eor(bool),
}

#[derive(Clone, Debug, Serialize, Deserialize, Any)]
pub enum RemoteOut {
    Command(String),
    Gmcp {
        command: String,
        data: Vec<u8>,
    },
    Msdp {
        name: String,
        values: Vec<MsdpValue>,
    },
}

#[derive(Clone, Debug, Serialize, Deserialize, Any)]
pub enum MsdpValue {
    Simple(Vec<u8>),
    Array(Vec<MsdpValue>),
    Table(HashMap<String, MsdpValue>),
}
