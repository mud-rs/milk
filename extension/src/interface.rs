use rune::Any;
use serde::{
    Deserialize,
    Serialize,
};

use crate::text::Lines;

#[derive(Clone, Debug, Serialize, Deserialize, Any)]
pub enum UiOut {
    Append { lines: Lines, outputs: Vec<Output> },
    SetContent { lines: Lines, output_id: u32 },
    SetPrompt { lines: Lines, output_id: u32 },
    SetEcho(bool),
}

#[derive(Clone, Debug, Serialize, Deserialize, Any)]
pub enum UiIn {
    Command { id: u32, command: String },
    Reload { id: u32 },
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize, Any)]
pub struct Output {
    pub id: u32,
    pub mode: Mode,
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize, Any)]
pub enum Mode {
    Fallback,
    Primary,
}
