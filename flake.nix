{
  description = "A Rust project using naersk";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    naersk = {
      url = "github:nix-community/naersk";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    fenix = {
      url = github:nix-community/fenix;
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, naersk, flake-utils, fenix }:
    let
      cargoToml = builtins.fromTOML (builtins.readFile ./client/Cargo.toml);
      name = cargoToml.package.name;
      version = cargoToml.package.version;
    in
    flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs {
            inherit system;
            overlays = [
              fenix.overlays.default
            ];
          };
          lib = pkgs.lib;
          toolchain = with pkgs.fenix;
            combine [
              (complete.withComponents [
                "cargo"
                "clippy"
                "rust-src"
                "rustc"
              ])
              targets.x86_64-pc-windows-gnu.latest.rust-std
              targets.x86_64-unknown-linux-musl.latest.rust-std
              targets.aarch64-unknown-linux-musl.latest.rust-std
            ];
          openssl-combined = pkgs.symlinkJoin {
            name = "openssl-combined";
            paths = with pkgs; [
              openssl.dev
              openssl.out
            ];
          };
          # Make naersk aware of the tool chain which is to be used.
          naersk-lib = naersk.lib.${system}.override {
            cargo = toolchain;
            rustc = toolchain;
          };
          buildPackage = target: { nativeBuildInputs ? [ ], ... }@args:
            naersk-lib.buildPackage (
              {
                inherit name version;
                src = ./.;
                doCheck = true;
                strictDeps = true;
              }
              // (lib.optionalAttrs (target != system) {
                CARGO_BUILD_TARGET = target;
              })
              // args
              // {
                OPENSSL_LIB_DIR = "${pkgs.openssl.out}/lib";
                OPENSSL_INCLUDE_DIR = "${pkgs.openssl.dev}/include";
                nativeBuildInputs = with pkgs; [
                  pkgs.fenix.complete.rustfmt-preview
                ] ++ nativeBuildInputs;
              }
            );
        in
        rec {
          packages = {
            default = buildPackage system { };
            x86_64-unknown-linux-musl = buildPackage "x86_64-unknown-linux-musl" {
              nativeBuildInputs = with pkgs; [ pkgsStatic.stdenv.cc ];
              CARGO_TARGET_X86_64_UNKNOWN_LINUX_MUSL_RUSTFLAGS = "-C target-feature=+crt-static";
            };
            aarch64-unknown-linux-musl = buildPackage "aarch64-unknown-linux-musl" {
              depsBuildBuild = with pkgs; [
                pkgsCross.aarch64-android.pkgsStatic.stdenv.cc
              ];

              CARGO_TARGET_AARCH64_UNKNOWN_LINUX_MUSL_RUSTFLAGS = "-C target-feature=+crt-static";
              CARGO_TARGET_AARCH64_UNKNOWN_LINUX_MUSL_LINKER = with
                pkgs.pkgsCross.aarch64-android.pkgsStatic.stdenv;
                "${cc}/bin/${cc.targetPrefix}gcc";
            };
            x86_64-pc-windows-gnu = buildPackage "x86_64-pc-windows-gnu" rec {
              doCheck = system == "x86_64-linux";

              depsBuildBuild = with pkgs; [
                pkgsCross.mingwW64.stdenv.cc
                pkgsCross.mingwW64.windows.pthreads
              ];

              nativeBuildInputs = lib.optional doCheck pkgs.wineWowPackages.stable;

              CARGO_TARGET_X86_64_PC_WINDOWS_GNU_RUNNER = pkgs.writeScript "wine-wrapper" ''
                # Without this, wine will error out when attempting to create the
                # prefix in the build's homeless shelter.
                export WINEPREFIX="$(mktemp -d)"
                exec wine64 $@
              '';
            };
          };

          apps.default = {
            type = "app";
            program = "${packages.default}/bin/milk";
          };

          devShells.default = pkgs.mkShell {
            OPENSSL_LIB_DIR = "${pkgs.openssl.out}/lib";
            OPENSSL_INCLUDE_DIR = "${pkgs.openssl.dev}/include";
            inputsFrom = builtins.attrValues packages;
            buildInputs = with pkgs; [
              rust-analyzer-nightly
              cargo-udeps
              nodejs
              deno
            ];
          };
        }
      ) // {
      hydraJobs = {
        inherit (self.packages) x86_64-linux;
      };
    };
}
