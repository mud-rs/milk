use std::{
    future::Future,
    path::Path,
    process::Stdio,
};

use anyhow::{
    anyhow,
    Error,
};
use mudrs_extension::{
    bus::{
        Bus,
        SharedClient,
    },
    interface::Mode,
};
use mudrs_text::AnsiProcessor;
use tokio::{
    io::{
        AsyncBufReadExt,
        AsyncWriteExt,
        BufReader,
    },
    process::Command,
    sync::oneshot,
};
use tracing::debug;
use url::Url;

const TS_MAIN: &str = include_str!("../ts/main.ts");

const DENO: &str = "deno";

fn url_to_addr(url: &Url) -> Result<String, Error> {
    Ok(match url.scheme() {
        "unix" => {
            let mut path: String = url.path().into();
            if let Some(host) = url.host() {
                path = format!("/{host}{path}");
            }
            path
        }
        "tcp" => format!("{}", url.port().unwrap_or(2517)),
        scheme => anyhow::bail!("unrecognized scheme: {}", scheme),
    })
}

pub async fn run(
    bus: impl Bus + Clone,
    path: impl AsRef<Path>,
    state_path: impl AsRef<Path>,
) -> Result<impl Future<Output = Result<(), Error>>, Error> {
    run_one(bus, path, state_path).await
}

async fn run_one(
    bus: impl Bus + Clone,
    path: impl AsRef<Path>,
    state_path: impl AsRef<Path>,
) -> Result<impl Future<Output = Result<(), Error>>, Error> {
    let client = SharedClient::new(bus.get_client("script.deno.host").await?);
    let url = bus
        .get_url()
        .ok_or_else(|| anyhow!("bus must be listening for typescript scripts"))
        .map(Clone::clone)?;
    let addr = url_to_addr(&url)?;
    let mut child = Command::new(DENO)
        .arg("run")
        .arg("--unstable")
        .arg("-A")
        .arg("-")
        .arg(path.as_ref())
        .arg(state_path.as_ref())
        .arg(&addr)
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()?;

    let (ready_tx, ready_rx) = oneshot::channel::<()>();

    let mut stdin = child.stdin.take().unwrap();
    stdin.write_all(TS_MAIN.as_bytes()).await?;
    drop(stdin);

    let stderr = child.stderr.take().unwrap();
    let mut stderr = BufReader::new(stderr).lines();
    tokio::task::spawn_local({
        let client = client.clone();
        async move {
            let mut writer = AnsiProcessor::new();
            while let Some(line) = stderr.next_line().await? {
                writer.append(line.as_bytes());
                let mut lines = writer.take();
                lines.complete_final();
                let _ = client.append(&[(0, Mode::Primary)], lines).await;
            }
            Result::<_, Error>::Ok(())
        }
    });

    let stdout = child.stdout.take().unwrap();
    let mut stdout = BufReader::new(stdout).lines();
    tokio::task::spawn_local({
        let client = client.clone();
        let mut ready_tx = Some(ready_tx);
        async move {
            let mut writer = AnsiProcessor::new();
            while let Some(line) = stdout.next_line().await? {
                if line.trim() == "ready" {
                    ready_tx.take();
                    continue;
                }
                writer.append(line.as_bytes());
                let mut lines = writer.take();
                lines.complete_final();
                let _ = client.append(&[(0, Mode::Primary)], lines).await;
            }
            Result::<_, Error>::Ok(())
        }
    });

    let _ = ready_rx.await;

    debug!("deno started");

    Ok(async move {
        let status = child.wait().await;

        debug!(?status, "deno exited");

        Ok(())
    })
}

#[cfg(test)]
mod tests {}
