import { Client, Frame, OP_PUBLISH } from "npm:busrt@0.1.2";

import { importModule } from "https://deno.land/x/import@v0.1.7/mod.ts";
import { exec } from "https://deno.land/x/exec@0.0.5/mod.ts";

declare global {
  let MudrsHost: ScriptHost;
  interface Window {
    MudrsHost: ScriptHost;
  }
}

type EventCallback = (topic: string, payload: string) => Promise<void>;
type ShutdownCallback = () => void;

interface ScriptHost {
  vars: unknown;
  state_dir: string;
  publish(topic: string, payload: string): Promise<void>;
  reload(): Promise<void>;
}

interface Module {
  on_event?: EventCallback;
  shutdown?: ShutdownCallback;
}

const script = Deno.args[0];
const state = Deno.args[1];
const addr = Deno.args[2];

if (script === undefined) {
  console.log("missing script argument");
  Deno.exit(1);
}

if (state === undefined) {
  console.log("missing addr argument");
  Deno.exit(1);
}

if (addr === undefined) {
  console.log("missing addr argument");
  Deno.exit(1);
}

class Queue<T> {
  private waiters: ((val: T | undefined) => void)[] = [];
  private items: T[] = [];

  private tick() {
    while (this.waiters.length > 0) {
      if (this.items.length > 0) {
        const next = this.waiters.shift();
        if (next === undefined) {
          throw new Error("Unexpected undefined in waiter list");
        }
        const item = this.items.shift();
        if (item === undefined) {
          throw new Error("Unexpected undefined in item list");
        }
        next(item);
      } else {
        break;
      }
    }
  }

  public next() {
    return new Promise<T | undefined>((resolve) => {
      this.waiters.push(resolve);
      queueMicrotask(this.tick.bind(this));
    });
  }

  public push(val: T) {
    this.items.push(val);
    this.tick();
  }
}

let frame_queue = new Queue<Frame>();

async function handle_frames() {
  while (true) {
    let frame = await frame_queue.next();
    if (on_event) {
      try {
        await on_event(frame.topic, frame.get_payload());
      } catch (e) {
        console.log("exception when handling event:", e);
      }
    }
  }
}
handle_frames();

class SysBus implements ScriptHost {
  busrt: Client;
  vars: unknown;
  state_dir: string;
  script: string;
  on_event?: EventCallback;

  constructor(bus: Client, state: string, script: string) {
    this.busrt = bus;
    this.state_dir = state;
    this.script = script;
  }

  async reload() {
    console.log("reloading scripts");
    let mod: Module;
    try {
      mod = await importModule(`file://${this.script}`, { force: true });
    } catch (e) {
      console.log("failed to load scripts:", e);
      return;
    }
    if (mod.on_event) {
      on_event = mod.on_event;
    } else {
      console.log("loaded script, but missing event handler");
    }
    if (shutdown) {
      shutdown();
    }
    if (mod.shutdown) {
      shutdown = mod.shutdown;
    }
  }

  async publish(topic: string, payload: string) {
    const frame = new Frame(OP_PUBLISH, 0);
    frame.topic = topic;
    frame.payload = payload;
    const op = await this.busrt.send(topic, frame);
    await op.wait_completed();
  }
}

let on_event: EventCallback | undefined;
let shutdown: ShutdownCallback | undefined;

async function main() {
  // connect
  const bus = new Client("script.deno");
  await bus.connect(addr);
  const op = await bus.subscribe(["remote.in", "ui.in"]);
  await op.wait_completed();

  window.MudrsHost = new SysBus(bus, state, script);
  await MudrsHost.reload();
  bus.on_frame = (frame: Frame) => frame_queue.push(frame);

  console.log("ready");

  const never = new Promise((_resolve, _reject) => {
    // do not call resolve or reject
  });
  await never;
}

main();
