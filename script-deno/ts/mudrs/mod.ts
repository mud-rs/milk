export type { Line, Output, Publish, RemoteIn, Span, UiIn } from "./proto.ts";
export { Lines, Spans } from "./proto.ts";

import {
  MatchAction,
  Matcher,
  Matchers,
  MatchResult,
  Stop,
} from "./matcher.ts";
import { Lines, Publish, revive, Spans } from "./proto.ts";

export type InitCallback = (bus: ScriptHost) => Promise<EventCallback>;
export type EventCallback = (topic: string, payload: string) => Promise<void>;

export async function on_event(_topic: string, payload: string) {
  const event: Publish = JSON.parse(payload, revive);
  if ("UiIn" in event && "Command" in event.UiIn) {
    const cmd = event.UiIn.Command.command;
    await Mudrs.publish({
      UiOut: {
        Append: {
          lines: Lines.withContent(cmd + "\n", { fg: "Magenta" }),
          outputs: [{ id: 0, mode: "Primary" }],
        },
      },
    });

    if (!(await Mudrs.aliases.run(cmd))) {
      await Mudrs.publish({
        RemoteOut: {
          Command: cmd,
        },
      });
    }
  }
  if ("UiIn" in event && "Reload" in event.UiIn) {
    await Mudrs.reload();
  }
  if ("RemoteIn" in event && "Lines" in event.RemoteIn) {
    const in_lines = event.RemoteIn.Lines;
    const out_lines = new Lines();
    for (const i in in_lines.full) {
      const line = in_lines.full[i];
      if (!(await Mudrs.triggers.run(line))) {
        out_lines.full.push(line);
      }
    }
    if (in_lines.final) {
      if (!(await Mudrs.triggers.run(in_lines.final))) {
        out_lines.full.push(in_lines.final);
      }
    }
    if (out_lines.full.length || out_lines.final) {
      await Mudrs.publish({
        UiOut: {
          Append: {
            lines: event.RemoteIn.Lines,
            outputs: [{
              id: 0,
              mode: "Primary",
            }],
          },
        },
      });
    }
  }
}

export interface ScriptHost {
  vars: unknown;
  state_dir: string;
  publish(topic: string, payload: string): Promise<void>;
  reload(): Promise<void>;
}

declare global {
  let MudrsHost: ScriptHost;
  interface Window {
    MudrsHost: ScriptHost;
  }
}

export class System {
  aliases: Matchers<string> = new Matchers();
  triggers: Matchers<Spans> = new Matchers();
  async reload() {
    await MudrsHost.reload();
  }

  async publish(event: Publish) {
    let topic = "";
    if ("UiOut" in event) {
      topic = "ui.out";
    }
    if ("UiIn" in event) {
      topic = "ui.in";
    }
    if ("RemoteOut" in event) {
      topic = "remote.out";
    }
    if ("RemoteIn" in event) {
      topic = "remote.in";
    }
    await MudrsHost.publish(topic, JSON.stringify(event));
  }

  async print(text: string) {
    const lines = Lines.withContent(`${text}\n`);
    await this.publish({
      UiOut: {
        Append: {
          lines,
          outputs: [{ id: 0, mode: "Primary" }],
        },
      },
    });
  }

  set_vars(vars: unknown) {
    MudrsHost.vars = vars;
  }

  vars<T>(): T {
    return MudrsHost.vars as T;
  }

  alias(
    pat: RegExp,
    action: MatchAction<string>,
  ): void {
    this.aliases.add(pat, action);
  }
  trigger(
    pat: RegExp,
    action: MatchAction<Spans>,
  ): void {
    this.triggers.add(pat, action);
  }
}

export const Mudrs: System = new System();

export interface Module {
  on_event?: EventCallback;
}
