import { MatchInput } from "./matcher.ts";

export type AnsiColor =
  | "Black"
  | "Red"
  | "Green"
  | "Yellow"
  | "Blue"
  | "Magenta"
  | "Cyan"
  | "White"
  | "BrightBlack"
  | "BrightRed"
  | "BrightGreen"
  | "BrightYellow"
  | "BrightBlue"
  | "BrightMagenta"
  | "BrightCan"
  | "BrightWhite";

export type Color = AnsiColor | {
  Indexed: boolean;
} | {
  Rgb: [number, number, number];
};

export interface Style {
  fg?: Color;
  bg?: Color;

  bold?: boolean;
  dim?: boolean;
  italic?: boolean;
  underlined?: boolean;
  slow_blink?: boolean;
  rapid_blink?: boolean;
  reversed?: boolean;
  hidden?: boolean;
  crossed_out?: boolean;
}

export interface Span {
  content: string;
  style?: Style;
}

class Range {
  start: number;
  end: number;

  constructor(start: number, end: number) {
    this.start = start;
    this.end = end;
  }

  contains(other: number): boolean {
    return other >= this.start && other < this.end;
  }
}

export class Spans extends Array<Span> implements MatchInput {
  toString(): string {
    let out = "";
    this.forEach((span) => {
      out += span.content;
    });
    return out;
  }
  slice(start?: number | undefined, end?: number | undefined): Span[] {
    const match_start = start ?? 0;
    const match_end = end ?? Infinity;

    if (start === end) {
      return [];
    }

    const match_range = new Range(match_start, match_end);

    const out: Span[] = [];

    let current = 0;

    this.forEach((span) => {
      const span_start = current;
      current += span.content.length;
      const span_end = current;

      const span_range = new Range(
        span_start,
        span_end,
      );
      if (
        !span_range.contains(match_start) &&
        !span_range.contains(match_end - 1) &&
        !match_range.contains(span_start) &&
        !match_range.contains(span_end - 1)
      ) {
        return;
      }

      let slice_start = 0;
      if (span_range.contains(match_start)) {
        slice_start = match_start - span_start;
      }

      let slice_end = span.content.length;
      if (span_range.contains(match_end)) {
        slice_end = match_end - span_start;
      }

      if (slice_start != slice_end) {
        out.push(
          {
            content: span.content.slice(slice_start, slice_end),
            style: span.style,
          },
        );
      }
    });

    return out;
  }
}

export type Line = Spans;

export class Lines {
  full: Line[];
  final?: Span[];

  constructor(p?: Partial<Lines>) {
    this.full = p?.full ?? [];
    this.final = p?.final;
  }

  static withContent(content: string, style?: Style): Lines {
    const full: Line[] = content.split("\n").map((l) => {
      return Object.assign(new Spans(), [{ content: l, style }]);
    });
    let final = full.pop();
    if (final && final[0].content.length == 0) {
      final = undefined;
    }

    return new Lines({ full, final });
  }
}

export interface Output {
  id: number;
  mode: Mode;
}

export type Mode = "Fallback" | "Primary";

export type UiOut =
  | {
    Append: {
      lines: Lines;
      outputs: Output[];
    };
  }
  | {
    SetContent: {
      lines: Lines;
      output_id: number;
    };
  }
  | {
    SetPrompt: {
      lines: Lines;
      output_id: number;
    };
  }
  | {
    SetEcho: boolean;
  };

export type UiIn =
  | {
    Command: {
      id: number;
      command: string;
    };
  }
  | {
    Reload: {
      id: number;
    };
  };

export type RemoteOut =
  | { Command: string }
  | {
    Gmcp: {
      command: string;
      data: Uint8Array;
    };
  }
  | {
    Msdp: {
      name: string;
      values: MsdpValue[];
    };
  };

export type RemoteIn =
  | { Lines: Lines }
  | {
    Gmcp: {
      command: string;
      data: Uint8Array;
    };
  }
  | {
    Msdp: {
      name: string;
      values: MsdpValue[];
    };
  }
  | { Echo: boolean }
  | { Eor: boolean };

export function revive(key: string, value: unknown): Lines | Line[] | unknown {
  if (key == "lines" && value instanceof Object && "full" in value) {
    return Object.assign(new Lines(), value);
  }

  if (key == "full" && value instanceof Array) {
    return value.map((v) => Object.assign(new Spans(), v));
  }

  return value;
}

export type MsdpValue =
  | { Simple: Uint8Array }
  | { Array: MsdpValue[] }
  | { Table: Map<string, MsdpValue> };

export type Publish =
  | { UiIn: UiIn }
  | { RemoteIn: RemoteIn }
  | { RemoteOut: RemoteOut }
  | { UiOut: UiOut };
