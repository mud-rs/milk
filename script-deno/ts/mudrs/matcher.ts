export const STOP = "Stop";
export type Stop = typeof STOP;

// Typed "constructor" for the STOP token.
// Helps with inference in async closures.
export function Stop(): Stop {
  return STOP;
}

export interface MatchInput {
  toString(): string;
  slice(start?: number, end?: number): ThisType<this>;
}

export class MatchResult<T> extends Array<T> {
  id: number;
  groups?: Record<string, T>;
  constructor(id: number) {
    super();

    this.id = id;
  }
}

interface Indices extends Array<[number, number]> {
  groups?: Record<string, [number, number]>;
}

declare global {
  interface RegExp {
    hasIndices: boolean;
  }

  interface RegExpExecArray {
    indices?: Indices;
  }
}

export type MatchAction<T> = (
  matched: MatchResult<T>,
) => Stop | void | Promise<Stop | void>;

export class Matcher<T extends MatchInput> {
  id: number;
  pattern: RegExp;
  action: MatchAction<T>;

  constructor(id: number, pattern: RegExp, action: MatchAction<T>) {
    if (!pattern.hasIndices) {
      pattern = new RegExp(pattern, pattern.flags + "d");
    }
    this.id = id;
    this.pattern = pattern;
    this.action = action;
  }

  match(input: T): MatchResult<T> | null {
    const unstyled = input.toString();
    const unstyled_result = this.pattern.exec(unstyled);
    if (!unstyled_result) {
      return null;
    }

    const result_indices = unstyled_result.indices!;

    const full_result = new MatchResult<T>(this.id);

    for (let n = 0; n < result_indices.length; n++) {
      const v = result_indices[n];
      if (!v) {
        continue;
      }
      const [i, j] = v;
      full_result[n] = input.slice(i, j) as T;
    }

    if (result_indices.groups) {
      full_result.groups = {};
      for (const k in result_indices.groups) {
        const v = result_indices.groups[k];
        if (!v) {
          continue;
        }
        const [i, j] = v;
        full_result.groups[k] = input.slice(i, j) as T;
      }
    }

    return full_result;
  }

  async run(input: T): Promise<Stop | void> {
    const matched = this.match(input);
    if (!matched) {
      return;
    }

    return await this.action(matched);
  }
}

export class Matchers<T extends MatchInput> {
  next_id = 0;
  matchers: { [key: number]: Matcher<T> } = {};

  add(pattern: RegExp, action: MatchAction<T>) {
    // Note: storing them using their negative ID so that iteration order is LIFO.
    this.matchers[-1 * this.next_id] = new Matcher(
      this.next_id,
      pattern,
      action,
    );
    this.next_id += 1;
  }
  remove(id: number) {
    delete this.matchers[id];
  }

  async run(input: T): Promise<Stop | void> {
    for (const id in this.matchers) {
      const matcher = this.matchers[id];
      try {
        const result = await matcher.run(input);
        if (result) {
          return result;
        }
      } catch (e) {
        throw new Error("matcher action failed", { cause: e });
      }
    }
  }
}
