#[macro_use]
mod helpers;

pub mod context;
pub mod diagnostics;
pub mod ext;
pub mod matcher;
pub mod system;
pub mod task;
pub mod text;
pub mod time;
pub mod ui;
pub mod worker;

pub use mudrs_extension::rune;
use rune::{
    compile::{
        ContextError,
        Module,
    },
    Context,
};
pub use rune_modules;
use worker::WorkerEnv;

pub fn module() -> Result<Module, ContextError> {
    let module = Module::with_crate("mudrs");
    Ok(module)
}

pub fn install_all(cx: &mut Context, env: WorkerEnv) -> Result<(), ContextError> {
    cx.install(&module()?)?;
    cx.install(&context::module()?)?;
    cx.install(&task::module()?)?;
    cx.install(&worker::module(env)?)?;
    cx.install(&matcher::core()?)?;
    cx.install(&matcher::specialized::<text::Spans>("trigger")?)?;
    cx.install(&matcher::specialized::<String>("alias")?)?;
    cx.install(&text::module()?)?;
    cx.install(&ext::module()?)?;
    cx.install(&ui::module()?)?;
    cx.install(&system::module()?)?;
    Ok(())
}

#[cfg(test)]
mod tests {

    use rune::Context;

    use super::*;

    #[test]
    fn test_register() -> Result<(), ContextError> {
        let mut cx = Context::new();
        install_all(&mut cx, Default::default()).unwrap();
        Ok(())
    }
}
