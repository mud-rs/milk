use std::{
    fmt::{
        self,
        Write,
    },
    ops::{
        Add,
        Sub,
    },
};

use rune::{
    runtime::{
        Protocol,
        VmError,
    },
    Any,
    ContextError,
    Module,
    Value,
};

/// Construct the `time` module.
pub fn module(_stdio: bool) -> Result<Module, ContextError> {
    let mut module = Module::with_crate("time");
    module.ty::<Duration>()?;
    module.function(&["Duration", "from_secs"], Duration::from_secs)?;
    module.inst_fn(Protocol::STRING_DEBUG, debug_duration)?;
    module.inst_fn(Protocol::STRING_DISPLAY, debug_duration)?;

    module.ty::<Instant>()?;
    module.function(&["Instant", "now"], Instant::now)?;
    module.inst_fn(Protocol::SUB, Instant::rune_sub)?;
    module.inst_fn(Protocol::ADD, <Instant as Add<Duration>>::add)?;
    module.inst_fn(Protocol::STRING_DEBUG, debug_instant)?;

    module.async_function(&["sleep"], sleep)?;
    Ok(module)
}

#[derive(Debug, Clone, Copy, Any)]
struct Duration {
    inner: tokio::time::Duration,
}

#[derive(Debug, Clone, Copy, Any)]
struct Instant {
    inner: tokio::time::Instant,
}

impl Instant {
    fn now() -> Self {
        Instant {
            inner: tokio::time::Instant::now(),
        }
    }

    fn rune_sub(&self, rhs: Value) -> Result<Value, VmError> {
        if let Value::Any(ref v) = rhs {
            if let Ok(d) = v.downcast_borrow_ref::<Duration>() {
                return Ok(Instant {
                    inner: self.inner - d.inner,
                }
                .into());
            }
            if let Ok(i) = v.downcast_borrow_ref::<Instant>() {
                return Ok(Duration {
                    inner: self.inner - i.inner,
                }
                .into());
            }
        }
        Err(VmError::bad_argument::<Instant>(0, &rhs)?)
    }
}

impl Sub<Instant> for Instant {
    type Output = Duration;

    fn sub(self, rhs: Self) -> Self::Output {
        Duration {
            inner: self.inner - rhs.inner,
        }
    }
}

impl Sub<Duration> for Instant {
    type Output = Instant;

    fn sub(self, rhs: Duration) -> Self::Output {
        Instant {
            inner: self.inner - rhs.inner,
        }
    }
}

impl Add<Duration> for Instant {
    type Output = Instant;

    fn add(self, rhs: Duration) -> Self::Output {
        Instant {
            inner: self.inner + rhs.inner,
        }
    }
}

impl Duration {
    /// Construct a duration from seconds.
    fn from_secs(secs: u64) -> Self {
        Self {
            inner: tokio::time::Duration::from_secs(secs),
        }
    }
}

fn debug_instant(instant: &Instant, buf: &mut String) -> fmt::Result {
    write!(buf, "{:?}", instant.inner)
}
fn debug_duration(duration: &Duration, buf: &mut String) -> fmt::Result {
    write!(buf, "{:?}", duration.inner)
}

/// Convert any value to a json string.
async fn sleep(duration: &Duration) {
    tokio::time::sleep(duration.inner).await;
}
