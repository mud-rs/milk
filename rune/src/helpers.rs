use crate::rune::runtime::{
    Args,
    Stack,
    Value,
    VmError,
};

#[cfg(test)]
macro_rules! prepare_source {
    ($cx:expr, $src:expr) => {{
        let mut diagnostics = crate::rune::Diagnostics::new();
        let result = crate::rune::prepare(&mut $src)
            .with_context(&$cx)
            .with_diagnostics(&mut diagnostics)
            .build();

        if !diagnostics.is_empty() {
            let mut writer = crate::rune::termcolor::StandardStream::stderr(
                crate::rune::termcolor::ColorChoice::Always,
            );
            diagnostics.emit(&mut writer, &$src)?;
        }

        std::sync::Arc::new(result?)
    }};
}

#[cfg(test)]
macro_rules! emit_errors {
    ($em:expr, $src:expr) => {{
        let mut writer = crate::rune::termcolor::StandardStream::stderr(
            crate::rune::termcolor::ColorChoice::Always,
        );
        $em.emit(&mut writer, &$src)?;
    }};
}

#[cfg(test)]
macro_rules! context_with {
	($($mod:expr),* $(,)*) => ({
        let mut cx = crate::rune::Context::with_default_modules()?;
        cx.install(&crate::rune_modules::io::module(true)?)?;
		$(
			cx.install(& $mod?)?;
		)*
		cx
	});
}

#[cfg(test)]
macro_rules! build_vm {
    ($($mod:expr),* $(,)* ; $src:expr) => {{
        let context = context_with!($($mod),*);
        let rcx = std::sync::Arc::new(context.runtime());
        let unit = prepare_source!(&context, $src);

        crate::rune::Vm::new(rcx, unit)
    }};
}

#[cfg(test)]
macro_rules! run_local {
	($vm:expr, $first:ident $(:: $rest:ident )* ($($arg:expr),*)) => (async {
        let local_set = tokio::task::LocalSet::new();

        let res = local_set.run_until($vm.async_call(&[stringify!($first), $(stringify!($rest),)*  ], ($($arg,)*))).await;

		local_set.await;
		res
	});
}

#[cfg(test)]
macro_rules! run_with {
	($($mod:expr),* $(,)* ; $first:ident $(:: $rest:ident )* ($($arg:expr),*)  ; $src:expr) => ({
        let mut src = $src;
        let mut vm = build_vm!($($mod),*; &mut src);
		match run_local!(vm, $first $(:: $rest)* ($($arg),*)).await {
            Ok(v) => Ok(v),
            Err(e) => {emit_errors!(e, src); Err(e)},
        }
	})
}

#[derive(Debug)]
pub struct ConcatArgs<A, B>(pub A, pub B);

impl<A, B> Args for ConcatArgs<A, B>
where
    A: Args,
    B: Args,
{
    fn count(&self) -> usize {
        self.0.count() + self.1.count()
    }

    fn into_stack(self, stack: &mut Stack) -> Result<(), VmError> {
        self.0.into_stack(stack)?;
        self.1.into_stack(stack)
    }

    fn into_vec(self) -> Result<Vec<Value>, VmError> {
        let mut v1 = self.0.into_vec()?;
        let mut v2 = self.1.into_vec()?;
        v1.append(&mut v2);
        Ok(v1)
    }
}
