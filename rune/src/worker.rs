use std::{
    cell::RefCell,
    fmt,
    fmt::Write,
    io,
    marker::PhantomData,
};

use futures::{
    channel::mpsc,
    prelude::*,
};
use thiserror::Error;
use tokio::task::{
    JoinError as TokioJoinError,
    JoinHandle as TokioJoinHandle,
    LocalSet,
};

use crate::{
    helpers::ConcatArgs,
    rune::{
        self,
        runtime::{
            Args,
            ConstValue,
            Future as RuneFuture,
            Protocol,
            SyncFunction,
            VmError,
            VmErrorKind,
        },
        Any,
        ContextError,
        Module,
        ToValue,
    },
};

pub struct WorkerEnv<F = fn() -> (), A = ()> {
    buffer: usize,
    args_factory: F,
    _args: PhantomData<fn() -> A>,
}

fn no_args() {}

impl Default for WorkerEnv<fn() -> (), ()> {
    fn default() -> Self {
        WorkerEnv {
            buffer: 0,
            args_factory: no_args,
            _args: PhantomData,
        }
    }
}

impl<F, A> WorkerEnv<F, A> {
    pub fn with_args_factory<G, B>(self, f: G) -> WorkerEnv<G, B>
    where
        G: Fn() -> B,
        B: Args,
    {
        WorkerEnv {
            buffer: self.buffer,
            args_factory: f,
            _args: PhantomData,
        }
    }
}

#[derive(Any, Debug)]
pub struct Channel {
    tx: RefCell<mpsc::Sender<ConstValue>>,
    rx: RefCell<mpsc::Receiver<ConstValue>>,
}

#[derive(Debug, Error, Any)]
#[error(transparent)]
pub struct SendError(#[from] mpsc::SendError);

impl SendError {
    fn debug(&self, buf: &mut String) -> fmt::Result {
        write!(buf, "{self:?}")
    }
    fn display(&self, buf: &mut String) -> fmt::Result {
        write!(buf, "{self}")
    }
}

impl Channel {
    pub async fn send(&self, value: ConstValue) -> Result<(), SendError> {
        let mut tx = self.tx.borrow().clone();
        Ok(tx.send(value).await?)
    }
    pub async fn recv(&self) -> Option<ConstValue> {
        #![allow(clippy::await_holding_refcell_ref)]
        self.rx.borrow_mut().next().await
    }
}

#[derive(Debug, ToValue)]
pub struct Worker {
    pub handle: JoinHandle,
    pub channel: Channel,
}

#[derive(Debug, Any, Error)]
#[error(transparent)]
pub struct JoinError(#[from] TokioJoinError);

impl JoinError {
    pub fn debug(&self, buf: &mut String) -> fmt::Result {
        write!(buf, "{self:?}")
    }
    pub fn display(&self, buf: &mut String) -> fmt::Result {
        write!(buf, "{self}")
    }
}

#[derive(Any, Debug)]
pub struct JoinHandle(TokioJoinHandle<Result<ConstValue, WorkerError>>);

impl JoinHandle {
    fn into_future(self) -> RuneFuture {
        RuneFuture::new(async {
            let res1 = self.0.await;
            match res1 {
                Ok(res2) => match res2 {
                    Ok(v) => Ok(Ok(v)),
                    Err(e) => match e {
                        WorkerError::CreateRuntime(_) => {
                            Err(VmError::from(VmErrorKind::NoRunningVm))
                        }
                        WorkerError::Vm(e) => Err(e),
                    },
                },
                Err(e) => Ok(Err(JoinError::from(e))),
            }
        })
    }

    fn abort(&self) {
        self.0.abort()
    }
}

#[derive(Error, Debug, Clone)]
#[error("error spawning worker")]
pub struct SpawnError {}

#[derive(Error, Debug)]
pub enum WorkerError {
    #[error("error joining task: {}", 0)]
    CreateRuntime(#[from] io::Error),
    #[error("error executing task: {}", 0)]
    Vm(#[from] VmError),
}

fn mk_start<F, A>(env: WorkerEnv<F, A>) -> impl Fn(SyncFunction) -> Worker
where
    F: Fn() -> A + Send + 'static,
    A: Args + Send + fmt::Debug + 'static,
{
    move |f| {
        let buffer = if env.buffer == 0 { 128 } else { env.buffer };
        let (parent_tx, child_rx) = mpsc::channel(buffer);
        let (child_tx, parent_rx) = mpsc::channel(buffer);
        let env_args = (env.args_factory)();
        let join = tokio::task::spawn_blocking(move || {
            let rt = tokio::runtime::Builder::new_current_thread()
                .enable_all()
                .build()?;

            let channel = Channel {
                tx: parent_tx.into(),
                rx: parent_rx.into(),
            };

            let args = ConcatArgs((channel,), env_args);

            let local_set = LocalSet::new();

            let res = local_set.block_on(&rt, f.async_send_call(args));
            rt.block_on(local_set);
            Ok(res?)
        });
        let handle = JoinHandle(join);
        let channel = Channel {
            tx: child_tx.into(),
            rx: child_rx.into(),
        };

        Worker { channel, handle }
    }
}

pub fn module<F, A>(env: WorkerEnv<F, A>) -> Result<Module, ContextError>
where
    F: Fn() -> A + Send + Sync + 'static,
    A: Args + fmt::Debug + Send + 'static,
{
    let mut module = Module::with_crate_item("mudrs", &["worker"]);
    module.ty::<JoinError>()?;
    module.inst_fn(Protocol::STRING_DEBUG, JoinError::debug)?;

    module.ty::<SendError>()?;
    module.inst_fn(Protocol::STRING_DISPLAY, SendError::display)?;
    module.inst_fn(Protocol::STRING_DEBUG, SendError::debug)?;

    module.ty::<Channel>()?;
    module.async_inst_fn("send", Channel::send)?;
    module.async_inst_fn("recv", Channel::recv)?;

    module.ty::<JoinHandle>()?;
    module.async_inst_fn(Protocol::INTO_FUTURE, JoinHandle::into_future)?;
    module.inst_fn("abort", JoinHandle::abort)?;

    module.function(&["start"], mk_start(env))?;

    Ok(module)
}

#[cfg(test)]
mod test {
    use rune::runtime::FromValue;

    use super::*;
    use crate::context::Global;

    #[tokio::test]
    async fn test_worker() -> Result<(), anyhow::Error> {
        let env = WorkerEnv::default();
        let v = run_with!(module(env); main(); rune::sources! {
            entry => {
                use mudrs::worker;
                async fn worker_fn(channel) {
                    let msg = channel.recv().await?;

                    channel.send("pong").await?;

                    "result"
                }

                pub async fn main() {
                    let child = worker::start(worker_fn);

                    child.channel.send("ping").await?;

                    let msg = child.channel.recv().await?;

                    let result = child.handle.await?;

                    result
                }
            }
        })?;

        assert_eq!(
            "result".to_string(),
            String::from_value(v).expect("a string result")
        );

        Ok(())
    }

    #[tokio::test]
    async fn test_worker_args() -> Result<(), anyhow::Error> {
        let global = Global::default();
        let env = WorkerEnv::default().with_args_factory({
            let global = global.clone();
            move || (global.clone(),)
        });
        let sources = rune::sources! {
            entry => {
                use mudrs::worker;
                async fn worker_fn(channel, global) {
                    let msg = channel.recv().await?;

                    let foo = global["foo"];

                    global["foo"] = foo + 5;

                    channel.send("pong").await?;
                }

                pub async fn main(global) {
                    let child = worker::start(worker_fn);

                    global["foo"] = 8;

                    child.channel.send("ping").await?;

                    child.channel.recv().await?;

                    child.handle.await?;

                    let foo = global["foo"];

                    foo
                }
            }
        };
        let v = run_with!(crate::context::module(), module(env);
            main(global); sources)?;

        assert_eq!(13, i64::from_value(v).expect("a number result"));

        Ok(())
    }

    #[tokio::test]
    async fn test_bad_worker() -> Result<(), anyhow::Error> {
        let sources = rune::sources! {
            entry => {
                use mudrs::worker;
                pub async fn main() {
                    let foo = || {
                        yield 5;
                        yield 10;
                    };
                    let worker = worker::start(async || {
                        println!("{}", foo.next())
                    });

                    worker.handle.await?;

                    println!("{}", foo.next());
                }
            }
        };

        let res = run_with!(module(Default::default()); main(); sources);

        assert!(res.is_err());

        Ok(())
    }
}
