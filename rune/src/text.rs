use std::{
    borrow::Cow,
    fmt,
    fmt::Write,
    ops::Range,
};

pub use ext::{
    Color,
    Line,
    Lines,
    Span,
    Spans,
    Style,
};
use mudrs_extension::text as ext;

use crate::{
    matcher::MatchInput,
    rune::{
        compile::{
            ContextError,
            Module,
        },
        runtime::Protocol,
    },
};

impl MatchInput for Spans {
    fn slice(&self, Range { start, end }: Range<usize>) -> Self {
        let mut out = vec![];

        if start == end {
            return Spans::default();
        }

        let match_range = Range { start, end };
        // println!("start: {}, end: {}", start, end);

        let mut current = 0;
        for s in &self.0 {
            let span_start = current;
            current += s.content.len();
            let span_end = current;
            // println!("current start: {}, current end: {}", span_start, span_end);
            let span_range = Range {
                start: span_start,
                end: span_end,
            };
            if !span_range.contains(&start)
                && !span_range.contains(&(end - 1))
                && !match_range.contains(&span_start)
                && !match_range.contains(&(span_end - 1))
            {
                // println!("not included, skipping");
                continue;
            }

            // println!("including at least part of this span");

            let slice_start = if span_range.contains(&start) {
                start - span_start
            } else {
                0
            };

            let slice_end = if span_range.contains(&end) {
                end - span_start
            } else {
                s.content.len()
            };

            out.push(Span {
                content: s.content[slice_start..slice_end].to_string(),
                style: s.style,
            })
        }

        Spans(out)
    }

    fn unstyled(&self) -> Cow<str> {
        let mut out = String::new();
        for s in &self.0 {
            out.push_str(s.content.as_str());
        }
        Cow::Owned(out)
    }
}

fn new_lines(full: Vec<Line>, r#final: Option<Spans>) -> Lines {
    Lines { full, r#final }
}

fn lines_from_string(input: String, style: Option<Style>) -> Lines {
    Lines::new(input, style.unwrap_or_default())
}

fn new_line(spans: Vec<Span>) -> Line {
    Line(spans)
}

fn new_spans(spans: Vec<Span>) -> Spans {
    Spans(spans)
}

fn new_span(content: String, style: Option<Style>) -> Span {
    Span {
        content,
        style: style.unwrap_or_default(),
    }
}

fn unstyled_spans(spans: &Spans) -> String {
    spans.unstyled().into()
}

fn debug_style(style: &Style, buf: &mut String) -> fmt::Result {
    write!(buf, "{style:?}")
}
fn debug_spans(spans: &Spans, buf: &mut String) -> fmt::Result {
    write!(buf, "{spans:?}")
}

pub fn module() -> Result<Module, ContextError> {
    let mut module = Module::with_crate_item("mudrs", &["text"]);

    module.ty::<Style>()?;
    module.inst_fn("clone", Style::clone)?;
    module.inst_fn(Protocol::STRING_DEBUG, debug_style)?;
    module.inst_fn("fg", Style::fg)?;
    module.inst_fn("bg", Style::bg)?;
    module.inst_fn("bold", Style::bold)?;
    module.function(&["Style", "new"], Style::default)?;

    module.ty::<Color>()?;
    module.inst_fn("clone", Color::clone)?;
    module.ty::<Span>()?;
    module.inst_fn("clone", Span::clone)?;
    module.function(&["Span", "new"], new_span)?;

    module.ty::<Spans>()?;
    module.inst_fn("clone", Spans::clone)?;
    module.inst_fn("unstyled", unstyled_spans)?;
    module.inst_fn("spans", |s: Spans| s.0)?;
    module.function(&["Spans", "new"], new_spans)?;
    module.inst_fn(Protocol::STRING_DEBUG, debug_spans)?;

    module.ty::<Line>()?;
    module.inst_fn("clone", Line::clone)?;
    module.inst_fn("spans", |s: Line| s.0)?;
    module.function(&["Line", "new"], new_line)?;

    module.ty::<Lines>()?;
    module.inst_fn("clone", Lines::clone)?;
    module.function(&["Lines", "new"], new_lines)?;
    module.function(&["Lines", "from_string"], lines_from_string)?;

    Ok(module)
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::rune;

    #[test]
    fn test_trigger_input() {
        let spans = Spans(vec![
            Span {
                content: "Hell".into(),
                style: Style {
                    fg: Some(Color::Red),
                    underlined: true,
                    ..Default::default()
                },
            },
            Span {
                content: "o, ".into(),
                ..Default::default()
            },
            Span {
                content: "world".into(),
                style: Style {
                    fg: Some(Color::Green),
                    bold: true,
                    ..Default::default()
                },
            },
            Span {
                content: "!".into(),
                ..Default::default()
            },
        ]);

        assert_eq!(spans.unstyled(), "Hello, world!");
        let first_slice = spans.slice(0..4);
        assert_eq!(first_slice[0], spans[0]);
        assert_eq!(first_slice.unstyled(), "Hell");
        assert_eq!(spans.slice(2..9).unstyled(), "llo, wo");
        assert_eq!(spans.slice(2..9).len(), 3);
        assert_eq!(
            spans.slice(0..spans.unstyled().len()).unstyled(),
            "Hello, world!"
        );
    }

    #[tokio::test]
    async fn test_spans() -> rune::Result<()> {
        let spans = Spans(vec![
            Span {
                content: "Hell".into(),
                style: Style {
                    fg: Some(Color::Red),
                    underlined: true,
                    ..Default::default()
                },
            },
            Span {
                content: "o, ".into(),
                ..Default::default()
            },
            Span {
                content: "world".into(),
                style: Style {
                    fg: Some(Color::Green),
                    bold: true,
                    ..Default::default()
                },
            },
            Span {
                content: "!".into(),
                ..Default::default()
            },
        ]);
        let _v = run_with!(module(); main(spans); rune::sources! {
            entry => {
                pub async fn main(spans) {
                    for span in spans.spans() {
                        println!("content: {}", span.content);
                        println!("style: {:?}", span.style);
                    }
                }
            }
        })?;

        Ok(())
    }

    use crate::matcher;

    #[tokio::test]
    async fn complicated_input() -> anyhow::Result<()> {
        const INPUT: &str = "\x1b[0m\x1b[0m\x1b[0m   1: \x1b[0m\x1b[1;33m[Cook Basic Ingredients]\x1b[0m\x1b[0m (200 experience, 300 riln, reps remaining: 1) We're short-staffed and could use some help cooking up some basic recipes to be used in meals prepared by our own staff.  You'll get temporary access to our kitchen's ingredients and tools, so no need to bring any of your own equipment.  \x1b[0m\x1b[1;33mCURRENT STEP:\x1b[0m\x1b[0m You must next cook: fried egg\x1b[0m";
        let mut ansi_processor = mudrs_text::AnsiProcessor::new();
        ansi_processor.append(INPUT.as_bytes());
        let mut lines = ansi_processor.take();

        let triggers = crate::matcher::Matchers::default();

        run_with!(module(), matcher::core(), matcher::specialized::<Spans>("trigger"); main(triggers.clone()); rune::sources! {
            entry => {
                pub async fn main(triggers) {
                    triggers.create(": *((?:fried|scrambled) egg)", async |m| {
                        println!("cookig: {}", m[1].unstyled());
                        None
                    });
                }
            }
        })?;

        triggers.run(lines.r#final.take().unwrap()).await.unwrap();

        Ok(())
    }

    #[tokio::test]
    async fn another_complicated_one() -> anyhow::Result<()> {
        const INPUT: &str =
            "\x1b[0m\x1b[0mYou're about to cook the following recipe: \x1b[0m\x1b[1;32mfried egg\x1b[0m\x1b[0m";

        let mut ansi_processor = mudrs_text::AnsiProcessor::new();
        ansi_processor.append(INPUT.as_bytes());
        let mut lines = ansi_processor.take();

        let triggers = crate::matcher::Matchers::default();

        run_with!(module(), matcher::core(), matcher::specialized::<Spans>("trigger"); main(triggers.clone()); rune::sources! {
            entry => {
                pub async fn main(triggers) {
                    triggers.create(": *((?:fried|scrambled) egg)", async |m| {
                        println!("cooking: {:?}", m[1]);
                        None
                    });
                }
            }
        })?;

        let input = lines.r#final.take().unwrap();

        println!("input: {input:#?}");

        triggers.run(input).await.unwrap();

        Ok(())
    }

    #[tokio::test]
    async fn yet_another_one() -> anyhow::Result<()> {
        const INPUT: &str = "<<<PLUGINATOR!>>>{'roundtime': 0, 'traveltime': 0, 'exp_bucket': 105, 'morale': 4.62, 'nutrition': 55, 'balance': -5, 'energy_current': 401, 'energy_max': 500, 'encumbrance': 1, 'sanity': 100, 'armor_level': 1, 'risk': 33}<<</PLUGINATOR!>>>";

        let mut ansi_processor = mudrs_text::AnsiProcessor::new();
        ansi_processor.append(INPUT.as_bytes());
        let mut lines = ansi_processor.take();

        let triggers = crate::matcher::Matchers::default();

        run_with!(module(), matcher::core(), matcher::specialized::<Spans>("trigger"); main(triggers.clone()); rune::sources! {
            entry => {
                pub async fn main(triggers) {
                    triggers.create("^(.*)<<<PLUGINATOR!>>>\\{(.*)\\}<<</PLUGINATOR!>>>(.*)$", async |m| {
                        println!("pre: {:?}", m[1]);
                        println!("post: {:?}", m[3]);
                        None
                    });
                }
            }
        })?;

        let input = lines.r#final.take().unwrap();

        println!("input: {input:#?}");

        triggers.run(input).await.unwrap();

        Ok(())
    }
}
