use std::{
    cell::RefCell,
    fmt,
    fmt::Write,
};

use futures::{
    channel::mpsc,
    prelude::*,
};
use thiserror::Error;
use tokio::task::{
    JoinError as TokioJoinError,
    JoinHandle as TokioHandle,
};

use crate::rune::{
    self,
    runtime::{
        Future as RuneFuture,
        Protocol,
        VmError,
    },
    Any,
    ContextError,
    Module,
    Value,
};

pub fn module() -> Result<Module, ContextError> {
    let mut module = Module::with_crate_item("mudrs", &["task"]);
    module.ty::<JoinError>()?;
    module.inst_fn(Protocol::STRING_DISPLAY, JoinError::display)?;
    module.inst_fn(Protocol::STRING_DEBUG, JoinError::debug)?;

    module.ty::<JoinHandle>()?;
    module.inst_fn("abort", JoinHandle::abort)?;
    module.inst_fn(Protocol::INTO_FUTURE, JoinHandle::into_future)?;

    module.ty::<SendError>()?;
    module.inst_fn(Protocol::STRING_DISPLAY, SendError::display)?;
    module.inst_fn(Protocol::STRING_DEBUG, SendError::debug)?;

    module.ty::<Channel>()?;
    module.async_inst_fn("send", Channel::send)?;
    module.async_inst_fn("next", Channel::next)?;
    module.function(&["Channel", "new"], || {
        let (tx1, rx1) = mpsc::channel(16);
        let (tx2, rx2) = mpsc::channel(16);
        (
            Channel {
                tx: tx1.into(),
                rx: rx2.into(),
            },
            Channel {
                tx: tx2.into(),
                rx: rx1.into(),
            },
        )
    })?;

    module.function(&["spawn"], spawn)?;
    Ok(module)
}

#[derive(Any, Debug)]
pub struct Channel {
    tx: RefCell<mpsc::Sender<Value>>,
    rx: RefCell<mpsc::Receiver<Value>>,
}

#[derive(Debug, Error, Any)]
#[error(transparent)]
pub struct SendError(#[from] mpsc::SendError);

impl SendError {
    fn debug(&self, buf: &mut String) -> fmt::Result {
        write!(buf, "{self:?}")
    }
    fn display(&self, buf: &mut String) -> fmt::Result {
        write!(buf, "{self}")
    }
}

impl Channel {
    pub async fn send(&self, value: Value) -> Result<(), SendError> {
        let mut tx = self.tx.borrow().clone();
        Ok(tx.send(value).await?)
    }
    pub async fn next(&self) -> Option<Value> {
        #![allow(clippy::await_holding_refcell_ref)]
        self.rx.borrow_mut().next().await
    }
}

#[derive(Error, Debug, Any)]
#[error(transparent)]
pub struct JoinError(#[from] TokioJoinError);

impl JoinError {
    fn display(&self, buf: &mut String) -> fmt::Result {
        write!(buf, "{self}")
    }
    fn debug(&self, buf: &mut String) -> fmt::Result {
        write!(buf, "{self:?}")
    }
}

#[derive(Debug, Any)]
pub struct JoinHandle(TokioHandle<Result<Value, VmError>>);

impl From<TokioHandle<Result<Value, VmError>>> for JoinHandle {
    fn from(inner: TokioHandle<Result<Value, VmError>>) -> Self {
        JoinHandle(inner)
    }
}

impl JoinHandle {
    fn abort(&self) {
        self.0.abort()
    }

    fn into_future(self) -> RuneFuture {
        RuneFuture::new(async {
            let res1 = self.0.await;
            let res: Result<Result<Value, JoinError>, VmError> = match res1 {
                Ok(res2) => match res2 {
                    Ok(v) => Ok(Ok(v)),
                    Err(e) => Err(e),
                },
                Err(e) => Ok(Err(e.into())),
            };
            res
        })
    }
}

pub fn spawn(f: RuneFuture) -> JoinHandle {
    tracing::debug!("spawning task");
    tokio::task::spawn_local(async {
        let res = f.await;
        tracing::debug!("task completed");
        res
    })
    .into()
}

#[cfg(test)]
mod test {

    use super::*;

    #[tokio::test]
    async fn test_spawn() -> Result<(), anyhow::Error> {
        let _res = run_with! (module(); main(); rune::sources! {
            entry => {
                use mudrs::task;

                pub async fn main() {
                    let task = task::spawn(async {
                        println!("Hello!");
                        5
                    });
                    let task2 = task::spawn(async {
                        println!("World!");
                        8
                    });
                    task.abort();
                    println!("main finished");

                    match task.await {
                        Err(e) => println!("error: {}", e),
                        Ok(v) => println!("task output: {}", v),
                    }
                    match task2.await {
                        Err(e) => println!("error: {}", e),
                        Ok(v) => println!("task output: {}", v),
                    }
                }
            }
        })?;
        Ok(())
    }
}
