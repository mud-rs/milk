use std::{
    collections::HashMap,
    sync::Arc,
};

use parking_lot::RwLock;

use crate::rune::{
    self,
    runtime::{
        ConstValue,
        Key,
        Object,
        Protocol,
        Shared,
        TypeOf,
        VmError,
        VmErrorKind,
    },
    Any,
    ContextError,
    Module,
};

#[derive(Any, Debug, Clone, Default)]
pub struct Global {
    inner: Arc<RwLock<HashMap<Key, ConstValue>>>,
}

impl Global {
    fn fallible_get(&self, name: Key) -> Result<ConstValue, VmError> {
        self.inner
            .read()
            .get(&name)
            .map(Clone::clone)
            .ok_or_else(|| {
                VmError::from(VmErrorKind::MissingIndexKey {
                    target: Global::type_info(),
                    index: name,
                })
            })
    }
    fn discard_set(&self, name: Key, value: ConstValue) {
        self.set(name, value);
    }

    fn get(&self, name: Key) -> Option<ConstValue> {
        self.inner.read().get(&name).map(Clone::clone)
    }

    fn set(&self, name: Key, value: ConstValue) -> Option<ConstValue> {
        self.inner.write().insert(name, value)
    }

    fn del(&self, name: Key) -> Option<ConstValue> {
        self.inner.write().remove(&name)
    }
}

pub struct Local {
    _inner: Shared<Object>,
}

pub fn module() -> Result<Module, ContextError> {
    let mut module = Module::with_crate("mudrs");
    module.ty::<Global>()?;
    module.inst_fn("get", Global::get)?;
    module.inst_fn("set", Global::set)?;
    module.inst_fn("del", Global::del)?;
    module.inst_fn(Protocol::INDEX_GET, Global::fallible_get)?;
    module.inst_fn(Protocol::INDEX_SET, Global::discard_set)?;
    Ok(module)
}

#[cfg(test)]
mod test {

    use rune::FromValue;

    use super::*;
    #[tokio::test]
    async fn test_global_context() -> Result<(), anyhow::Error> {
        let cx = Global::default();

        let mut vm = build_vm!(module(); rune::sources! {
            entry => {
                pub async fn set(cx, name, value) {
                    cx.set(name, value);
                }
                pub async fn get(cx, name) {
                    cx.get(name)
                }
                pub async fn main() {}
            }
        });

        let handle = tokio::runtime::Handle::current().spawn_blocking({
            let rcx = vm.context().clone();
            let unit = vm.unit().clone();
            let cx = cx.clone();
            move || -> Result<(), anyhow::Error> {
                let mut vm = rune::Vm::new(rcx, unit);
                let fut = run_local!(vm, set(&cx, "spam", "eggs"));
                tokio::runtime::Builder::new_current_thread()
                    .enable_all()
                    .build()?
                    .block_on(fut)?;
                Ok(())
            }
        });

        run_local!(vm, main()).await?;
        run_local!(vm, set(&cx, "foo", "bar")).await?;
        handle.await??;
        let res = run_local!(vm, get(&cx, "foo")).await?;
        assert_eq!(Option::<String>::from_value(res)?.as_deref(), Some("bar"));
        let res = run_local!(vm, get(&cx, "spam")).await?;
        assert_eq!(Option::<String>::from_value(res)?.as_deref(), Some("eggs"));

        Ok(())
    }

    #[tokio::test]
    async fn test_proto_fns() -> Result<(), anyhow::Error> {
        let cx = Global::default();
        run_with!(module(); main(cx); rune::sources! {
            entry => {
                pub fn main(global) {
                    global["foo"] = "bar";

                    println!("{:?}", global["foo"]);
                }
            }
        })?;
        Ok(())
    }
}
