use mudrs_extension::{
    bus::*,
    interface::{
        UiIn,
        UiOut,
    },
    remote::{
        RemoteIn,
        RemoteOut,
    },
};

use crate::rune::{
    ContextError,
    Module,
};

pub fn module() -> Result<Module, ContextError> {
    let mut module = Module::with_crate_item("mudrs", &["ext"]);

    module.ty::<SharedClient>()?;
    module.async_inst_fn("publish", SharedClient::publish)?;

    module.ty::<RemoteIn>()?;
    module.ty::<RemoteOut>()?;
    module.ty::<UiIn>()?;
    module.ty::<UiOut>()?;

    Ok(module)
}
