use std::{
    path::PathBuf,
    sync::Arc,
};

use mudrs_extension::{
    self,
    bus::SharedClient,
    rune::runtime::Value,
    text::{
        Lines,
        Spans,
    },
};

use crate::{
    matcher::{
        MatcherID,
        Matchers,
    },
    rune::{
        self,
        compile::ContextError,
        runtime::{
            Function,
            Protocol,
        },
        Any,
        Context,
        Module,
    },
    ui::Mode,
};

#[derive(Clone, Any)]
pub struct System {
    pub context: Arc<Context>,
    pub source_path: PathBuf,
    pub state_path: PathBuf,

    pub trigger_matchers: Matchers<Spans>,
    pub alias_matchers: Matchers<String>,

    pub vars: Value,

    pub client: SharedClient,
}

impl System {
    pub fn alias(&self, pat: &str, action: Function) -> rune::Result<MatcherID> {
        self.alias_matchers.create(pat, action)
    }
    pub fn trigger(&self, pat: &str, action: Function) -> rune::Result<MatcherID> {
        self.trigger_matchers.create(pat, action)
    }
    pub fn unalias(&self, id: MatcherID) {
        self.alias_matchers.remove(id);
    }
    pub fn untrigger(&self, id: MatcherID) {
        self.trigger_matchers.remove(id);
    }
    pub async fn send(&self, cmd: String) -> rune::Result<()> {
        self.client.send(cmd).await
    }
    pub async fn set_prompt(&mut self, id: u32, lines: Lines) -> rune::Result<()> {
        self.client.set_prompt(id, lines).await
    }
    pub async fn append(&self, outputs: Vec<(i64, Mode)>, lines: Lines) -> rune::Result<()> {
        self.client
            .append(
                outputs
                    .into_iter()
                    .map(|(n, m)| (n as u32, m.into()))
                    .collect::<Vec<_>>()
                    .as_slice(),
                lines,
            )
            .await
    }
    pub async fn set_content(&self, output_id: i64, lines: Lines) -> rune::Result<()> {
        self.client.set_content(output_id as _, lines).await
    }
}

pub fn module() -> Result<Module, ContextError> {
    let mut module = Module::with_crate("mudrs");

    module.ty::<System>()?;
    module.field_fn(Protocol::GET, "vars", |s: &System| s.vars.clone())?;
    module.field_fn(Protocol::GET, "triggers", |s: &System| {
        s.trigger_matchers.clone()
    })?;
    module.field_fn(Protocol::GET, "aliases", |s: &System| {
        s.alias_matchers.clone()
    })?;
    module.field_fn(Protocol::GET, "client", |s: &System| s.client.clone())?;
    module.field_fn(Protocol::GET, "state_path", |s: &System| {
        format!("{}", s.state_path.display())
    })?;
    module.async_inst_fn("set_prompt", System::set_prompt)?;
    module.async_inst_fn("append", System::append)?;
    module.async_inst_fn("set_content", System::set_content)?;
    module.async_inst_fn("send", System::send)?;
    module.inst_fn("alias", System::alias)?;
    module.inst_fn("trigger", System::trigger)?;
    module.inst_fn("unalias", System::unalias)?;
    module.inst_fn("untrigger", System::untrigger)?;

    Ok(module)
}
