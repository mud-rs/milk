use mudrs_extension::interface::Mode as ExtMode;
use rune::Any;

use crate::rune::{
    ContextError,
    Module,
};

#[derive(Any, Clone, Copy, Debug)]
pub enum Mode {
    #[rune(constructor)]
    Primary,
    #[rune(constructor)]
    Fallback,
}

impl From<Mode> for ExtMode {
    fn from(other: Mode) -> Self {
        match other {
            Mode::Primary => ExtMode::Primary,
            Mode::Fallback => ExtMode::Fallback,
        }
    }
}

pub fn module() -> Result<Module, ContextError> {
    let mut module = Module::with_crate_item("mudrs", &["ui"]);

    module.ty::<Mode>()?;
    module.inst_fn("clone", Mode::clone)?;

    Ok(module)
}
