use std::{
    borrow::Cow,
    cell::RefCell,
    collections::{
        BTreeMap,
        HashMap,
    },
    fmt::{
        self,
        Write,
    },
    marker::PhantomData,
    ops::Range,
    rc::Rc,
    sync::Arc,
};

use rayon::prelude::*;
use regex::{
    CaptureLocations,
    Regex,
};

use crate::rune::{
    self,
    compile::{
        ContextError,
        Module,
        Named,
    },
    runtime::{
        Function,
        Key,
        Protocol,
        ToValue,
        TypeOf,
        UnsafeFromValue,
        Value,
        VmError,
        VmErrorKind,
    },
    Any,
    FromValue,
};

#[derive(Any, Default, Debug, Clone)]
pub struct Matchers<I: MatchInput> {
    inner: Rc<RefCell<Inner>>,
    _ph: PhantomData<fn(I)>,
}

#[derive(Any, Debug)]
pub struct MatcherID(usize);

impl MatcherID {
    fn display(&self, buf: &mut String) -> fmt::Result {
        write!(buf, "{}", self.0)
    }
}

impl<I: MatchInput> Matchers<I> {
    pub fn create(&self, pattern: &str, action: Function) -> rune::Result<MatcherID> {
        Ok(MatcherID(self.inner.borrow_mut().create(pattern, action)?))
    }

    pub fn remove(&self, id: MatcherID) {
        self.inner.borrow_mut().remove(id.0);
    }

    pub async fn run(&self, line: I) -> Result<bool, VmError> {
        let unstyled = line.unstyled();
        let matches = self.inner.borrow().find_matches(&unstyled);
        for (action_id, captures, trigger) in matches.into_iter().rev() {
            let tm = Match {
                input: line.clone(),
                trigger,
                captures,
            };
            let action = if let Some(a) = self.inner.borrow().actions.get(&action_id) {
                a.clone()
            } else {
                continue;
            };
            match action.run(tm).await? {
                MatchResult::Continue => continue,
                MatchResult::Stop => return Ok(true),
                MatchResult::Error(e) => tracing::error!(error = %e, "matcher returned error"),
            }
        }

        Ok(false)
    }

    pub fn clear(&self) {
        self.inner.borrow_mut().clear();
    }
}

pub trait MatchInput: 'static + Clone + Named + UnsafeFromValue + ToValue + Send {
    fn slice(&self, range: Range<usize>) -> Self;
    fn unstyled(&self) -> Cow<str>;
}

impl MatchInput for String {
    fn slice(&self, range: Range<usize>) -> Self {
        self[range].to_string()
    }

    fn unstyled(&self) -> Cow<str> {
        Cow::Borrowed(self)
    }
}

impl MatchInput for &'static str {
    fn slice(&self, range: Range<usize>) -> Self {
        &self[range]
    }

    fn unstyled(&self) -> Cow<str> {
        Cow::Borrowed(self)
    }
}

#[derive(Default, Debug)]
struct Inner {
    // The current ID used when adding a trigger.
    // Always increments, never reused.
    current_id: usize,

    triggers: BTreeMap<usize, Arc<Trigger>>,

    actions: HashMap<usize, MatchAction>,
}

impl Inner {
    fn add(&mut self, regex: Regex, text: &str, action: Function) -> usize {
        let id = self.current_id;
        self.current_id += 1;
        let mut named_captures = HashMap::new();
        for (idx, name) in regex.capture_names().enumerate() {
            if let Some(name) = name {
                let key = name.to_string();
                named_captures.insert(key.into(), idx);
            }
        }
        self.actions.insert(id, MatchAction(Rc::new(action)));
        self.triggers.insert(
            id,
            Arc::new(Trigger {
                id,
                regex,
                named_captures,
                text: text.into(),
            }),
        );
        id
    }

    pub fn create(&mut self, pattern: &str, action: Function) -> rune::Result<usize> {
        let regex = regex::Regex::new(pattern)?;
        Ok(self.add(regex, pattern, action))
    }

    pub fn remove(&mut self, id: usize) -> Option<Arc<Trigger>> {
        self.triggers.remove(&id)
    }

    pub fn clear(&mut self) {
        self.triggers.clear();
        self.actions.clear();
        self.current_id = 0;
    }

    pub fn find_matches(&self, unstyled: &str) -> Vec<(usize, CaptureLocations, Arc<Trigger>)> {
        self.triggers
            .par_iter()
            .filter_map(move |(id, p)| {
                let mut locs = p.regex.capture_locations();
                p.regex.captures_read(&mut locs, unstyled)?;
                Some((*id, locs, p.clone()))
            })
            .collect()
    }
}

#[derive(Debug, Clone)]
struct MatchAction(Rc<Function>);

#[derive(Debug, Any)]
pub struct Trigger {
    pub id: usize,
    pub text: String,
    pub regex: Regex,
    pub named_captures: HashMap<Key, usize>,
}

#[derive(Debug, Any)]
pub struct Match<I: MatchInput> {
    pub input: I,
    pub trigger: Arc<Trigger>,
    pub captures: CaptureLocations,
}

impl MatchAction {
    async fn run<I>(&self, trigger_match: Match<I>) -> Result<MatchResult, VmError>
    where
        I: MatchInput,
    {
        self.0.async_send_call((trigger_match,)).await
    }
}

impl<I> Match<I>
where
    I: MatchInput,
{
    fn id(&self) -> MatcherID {
        MatcherID(self.trigger.id)
    }

    fn get(&self, idx: Key) -> Option<I> {
        match idx {
            Key::String(_) => self.get_named(&idx),
            Key::Integer(i) => self.get_idx(i as _),
            _ => None,
        }
    }

    fn get_idx(&self, idx: usize) -> Option<I> {
        let (start, end) = self.captures.get(idx)?;
        Some(self.input.slice(start..end))
    }

    fn get_named(&self, name: &Key) -> Option<I> {
        let idx = *self.trigger.named_captures.get(name)?;
        self.get_idx(idx)
    }

    fn fallible_get(&self, idx: Key) -> Result<I, VmError> {
        let m = match idx {
            Key::String(_) => self.get_named(&idx),
            Key::Integer(i) => self.get_idx(i as _),
            _ => {
                return Err(VmError::from(VmErrorKind::KeyNotSupported {
                    actual: idx.type_info(),
                }))
            }
        };
        m.ok_or_else(|| {
            VmError::from(VmErrorKind::MissingIndexKey {
                target: Self::type_info(),
                index: idx,
            })
        })
    }
}

#[derive(Debug)]
pub enum MatchResult {
    Stop,
    Continue,
    Error(anyhow::Error),
}

impl FromValue for MatchResult {
    fn from_value(value: Value) -> Result<Self, VmError> {
        match value {
            Value::Unit => Ok(MatchResult::Continue),
            Value::Option(opt) => match opt.take()? {
                Some(v) => <Self as FromValue>::from_value(v),
                None => Ok(MatchResult::Stop),
            },
            Value::Result(res) => match res.take()? {
                Ok(v) => <Self as FromValue>::from_value(v),
                Err(e) => Ok(MatchResult::Error(
                    <anyhow::Error as FromValue>::from_value(e)?,
                )),
            },
            _ => Err(VmError::expected::<()>(value.type_info()?)),
        }
    }
}

pub fn specialized<I>(name: &str) -> Result<Module, ContextError>
where
    I: MatchInput,
{
    let mut module = Module::with_crate_item("mudrs", &[name]);
    module.ty::<Match<I>>()?;
    module.inst_fn("id", Match::<I>::id)?;
    module.inst_fn("get", Match::<I>::get)?;
    module.inst_fn(Protocol::INDEX_GET, Match::<I>::fallible_get)?;

    module.ty::<Matchers<I>>()?;
    module.inst_fn("create", Matchers::<I>::create)?;
    module.inst_fn("remove", Matchers::<I>::remove)?;
    module.async_inst_fn("run", Matchers::<I>::run)?;

    Ok(module)
}

pub fn core() -> Result<Module, ContextError> {
    let mut module = Module::with_crate_item("mudrs", &["matcher"]);

    module.ty::<MatcherID>()?;
    module.inst_fn(Protocol::STRING_DISPLAY, MatcherID::display)?;

    module.constant(&["STOP"], Option::<()>::None)?;

    Ok(module)
}

#[cfg(test)]
mod test {
    use std::path::PathBuf;

    use super::*;

    #[tokio::test]
    async fn test_trigger() -> rune::Result<()> {
        let triggers = Matchers::default();
        let mut sources = rune::Sources::new();
        let test_source = rune::Source::from_path(PathBuf::from("./src/trigger_test.rn").as_ref())?;
        sources.insert(test_source);
        run_with!(core(), specialized::<&'static str>("trigger"); main(triggers.clone()); sources)?;

        let input = "Hello, world!";
        assert!(triggers.run(input).await?);

        assert!(triggers.run("world is dumb.").await?);

        Ok(())
    }
}
