//! Holy shit this is a mess, and only getting worse as I hack more things into
//! it.
//!

use std::{
    cell::RefCell,
    path::{
        Path,
        PathBuf,
    },
    rc::Rc,
    sync::{
        atomic::{
            AtomicBool,
            Ordering,
        },
        Arc,
    },
};

use anyhow::Error;
use futures::prelude::*;
use mudrs_extension::{
    bus::{
        Bus,
        Event,
        Events,
        PublishEvent,
        SharedClient,
        Topic,
    },
    interface::{
        Mode,
        UiIn,
    },
    remote::RemoteIn,
    rune::FromValue,
    text::{
        Color,
        Lines,
        Spans,
        Style,
    },
};
use mudrs_rune::{
    matcher::Matchers,
    rune::{
        self,
        compile::Module,
        runtime::{
            Object,
            Panic,
            Shared,
            Stack,
            Value,
            VmError,
        },
        ContextError,
    },
    system::System,
};
use mudrs_text::AnsiProcessor;
use tokio::sync::{
    mpsc::{
        self,
        UnboundedSender,
    },
    oneshot::{
        self,
        Sender,
    },
    Mutex,
};
use tokio_stream::wrappers::UnboundedReceiverStream;
#[allow(unused_imports)]
use tracing::{
    debug,
    error,
    info,
    warn,
};

type IoSender = UnboundedSender<(String, Sender<Result<(), Error>>)>;

fn print_impl(sender: &IoSender, m: &str) -> Result<(), Panic> {
    let (tx, rx) = oneshot::channel();
    sender.send((m.to_string(), tx)).map_err(Panic::custom)?;
    futures::executor::block_on(rx)
        .map_err(Panic::custom)
        .and_then(|r| r.map_err(Panic::custom))
}

fn println_impl(sender: &IoSender, m: &str) -> Result<(), Panic> {
    let (tx, rx) = oneshot::channel();
    sender.send((format!("{m}\n"), tx)).map_err(Panic::custom)?;
    futures::executor::block_on(rx)
        .map_err(Panic::custom)
        .and_then(|r| r.map_err(Panic::custom))
}

fn dbg_impl(sender: &IoSender, stack: &mut Stack, args: usize) -> Result<(), VmError> {
    for value in stack.drain(args)? {
        let (tx, rx) = oneshot::channel();
        sender
            .send((format!("{value:?}\n"), tx))
            .map_err(VmError::panic)?;
        futures::executor::block_on(rx)
            .map_err(VmError::panic)
            .and_then(|r| r.map_err(VmError::panic))?;
    }

    stack.push(Value::Unit);
    Ok(())
}

fn io_module(sender: IoSender) -> Result<Module, ContextError> {
    let mut module = Module::with_crate_item("std", &["io"]);

    module.function(&["print"], {
        let sender = sender.clone();
        move |s: &str| print_impl(&sender, s)
    })?;
    module.function(&["println"], {
        let sender = sender.clone();
        move |s: &str| println_impl(&sender, s)
    })?;
    module.raw_fn(&["dbg"], {
        let sender = sender;
        move |s: &mut Stack, n: usize| dbg_impl(&sender, s, n)
    })?;

    Ok(module)
}

fn fs() -> Result<Module, ContextError> {
    let mut module = Module::with_crate("fs");

    module.function(
        &["write_string"],
        |path: &str, content: &str| -> rune::Result<()> { Ok(std::fs::write(path, content)?) },
    )?;
    module.function(&["read_to_string"], |path: &str| -> rune::Result<String> {
        Ok(std::fs::read_to_string(path)?)
    })?;

    Ok(module)
}

pub async fn run(
    bus: impl Bus,
    path: impl AsRef<Path>,
    state_path: impl AsRef<Path>,
) -> Result<(), anyhow::Error> {
    let mut client = bus.get_client("script.rune").await?;
    client
        .subscribe_bulk(&[Topic::RemoteIn, Topic::UiIn])
        .await?;
    let events = client.events().unwrap();

    let client = SharedClient(Arc::new(Mutex::new(client)));

    let (io_tx, mut io_rx): (IoSender, _) = mpsc::unbounded_channel();
    tokio::spawn({
        let client = client.clone();
        async move {
            while let Some((s, resp)) = io_rx.recv().await {
                let lines = Lines::new(s, Default::default());
                let res = client.append(&[(0, Mode::Primary)], lines).await;
                let _ = resp.send(res.map(|_| ()));
            }
        }
    });

    let (ui_rx, remote_rx) = fanout(events);

    let vars = Value::Object(Shared::new(Object::default()));

    let (triggers, aliases, sources) = load(
        &state_path,
        &path,
        io_tx.clone(),
        client.clone(),
        vars.clone(),
    )
    .await?;

    let triggers = Rc::new(RefCell::new(triggers));
    let aliases = Rc::new(RefCell::new(aliases));

    let sources = Arc::new(arc_swap::ArcSwap::from(sources));

    let echo = Arc::new(AtomicBool::new(true));

    tokio::task::spawn_local(run_remote(
        echo.clone(),
        client.clone(),
        triggers.clone(),
        sources.clone(),
        remote_rx,
    ));

    let local = tokio::task::spawn_local(run_local(
        echo,
        client,
        triggers,
        aliases,
        sources,
        state_path.as_ref().into(),
        path.as_ref().into(),
        ui_rx,
        io_tx,
        vars,
    ));

    let _ = local.await;

    info!("exiting script task");

    Ok(())
}

async fn load<P, S>(
    state_path: P,
    source_path: S,
    io_tx: IoSender,
    client: SharedClient,
    vars: Value,
) -> Result<(Matchers<Spans>, Matchers<String>, Arc<rune::Sources>), anyhow::Error>
where
    P: AsRef<Path>,
    S: AsRef<Path>,
{
    let mut context = rune::Context::with_config(false)?;
    mudrs_rune::install_all(&mut context, Default::default())?;
    context.install(&mudrs_rune::time::module(false)?)?;
    context.install(&rune_modules::io::module(false)?)?;
    context.install(&rune_modules::fmt::module(false)?)?;
    context.install(&rune_modules::rand::module(false)?)?;
    context.install(&rune_modules::core::module(false)?)?;
    context.install(&rune_modules::json::module(false)?)?;
    context.install(&fs()?)?;
    context.install(&io_module(io_tx)?)?;
    let context = Arc::new(context);

    let triggers = Matchers::default();
    let aliases = Matchers::default();

    let system = System {
        context: context.clone(),
        state_path: state_path.as_ref().into(),
        source_path: source_path.as_ref().into(),
        trigger_matchers: triggers.clone(),
        alias_matchers: aliases.clone(),
        client: client.clone(),
        vars,
    };

    let mut sources = rune::Sources::new();
    sources.insert(rune::Source::from_path(source_path.as_ref())?);

    let mut diagnostics = rune::Diagnostics::new();
    let result = rune::prepare(&mut sources)
        .with_context(&context)
        .with_diagnostics(&mut diagnostics)
        .build();

    let sources = Arc::new(sources);

    if !diagnostics.is_empty() {
        let mut writer = AnsiProcessor::new();
        diagnostics.emit(&mut writer, &sources)?;
        let _ = client.append(&[(0, Mode::Primary)], writer.take()).await;
    }

    let rt_ctx = Arc::new(context.runtime());

    if let Ok(unit) = result {
        let unit = Arc::new(unit);
        let mut vm = rune::Vm::new(rt_ctx.clone(), unit.clone());
        let res = vm.async_call(&["init"], (system,)).await;
        match res {
            Ok(res) => {
                let msg = if let Value::Result(_) = res {
                    let rune_res =
                        rune::Result::<Value>::from_value(res).expect("should be a result");
                    if let Err(e) = rune_res {
                        Some(format!("init returned an error: {e}\n"))
                    } else {
                        None
                    }
                } else if let Value::Unit = res {
                    None
                } else {
                    Some(format!("init returned an unexpected result: {res:?}\n"))
                };
                if let Some(msg) = msg {
                    let _ = client
                        .append(&[(0, Mode::Primary)], Lines::new(msg, Default::default()))
                        .await;
                } else {
                    return Ok((triggers, aliases, sources));
                }
            }
            Err(e) => {
                warn!(?e, "error loading scripts");
                let mut writer = AnsiProcessor::new();
                e.emit(&mut writer, &sources)?;
                let _ = client.append(&[(0, Mode::Primary)], writer.take()).await;
            }
        }
    }

    Ok(Default::default())
}

fn fanout(
    mut events: Events,
) -> (
    UnboundedReceiverStream<UiIn>,
    UnboundedReceiverStream<RemoteIn>,
) {
    let (remote_tx, remote_rx) = mpsc::unbounded_channel();
    let (ui_tx, ui_rx) = mpsc::unbounded_channel();
    tokio::spawn(async move {
        while let Some(event) = events.next().await {
            let event = match event {
                Ok(e) => e,
                Err(e) => {
                    error!("invalid event received: {}", e);
                    continue;
                }
            };

            let Event::Publish(event) = event;

            let res: Result<(), Error> = match event {
                PublishEvent::UiIn(event) => ui_tx.send(event).map_err(Into::into),
                PublishEvent::RemoteIn(event) => remote_tx.send(event).map_err(Into::into),
                other => {
                    warn!("unhandled event: {:?}", other);
                    continue;
                }
            };

            if let Err(e) = res {
                warn!("error sending event to task: {}", e);
            }
        }
    });
    (
        UnboundedReceiverStream::new(ui_rx),
        UnboundedReceiverStream::new(remote_rx),
    )
}

#[allow(clippy::await_holding_refcell_ref)]
async fn run_remote(
    echo: Arc<AtomicBool>,
    client: SharedClient,
    triggers: Rc<RefCell<Matchers<Spans>>>,
    sources: Arc<arc_swap::ArcSwap<rune::Sources>>,
    mut remote_rx: UnboundedReceiverStream<RemoteIn>,
) {
    while let Some(msg) = remote_rx.next().await {
        match msg {
            RemoteIn::Lines(lines) => {
                for line in lines.full {
                    let handled: bool = match triggers.borrow().run(Spans(line.0.clone())).await {
                        Ok(h) => h,
                        Err(e) => {
                            let mut writer = AnsiProcessor::new();
                            e.emit(&mut writer, &sources.load())
                                .expect("unable to emit diagnostics");
                            let _ = client.append(&[(0, Mode::Primary)], writer.take()).await;
                            false
                        }
                    };
                    if !handled {
                        let _ = client
                            .append(
                                &[(0, Mode::Primary)],
                                Lines {
                                    full: vec![line],
                                    ..Default::default()
                                },
                            )
                            .await;
                    }
                }
            }
            RemoteIn::Echo(value) => {
                echo.store(value, Ordering::Relaxed);
                let _ = client.set_echo(value).await;
            }
            other => {
                debug!(?other, "mtype currently unhandled");
            }
        }
    }
    let _ = client
        .append(
            &[(0, Mode::Primary)],
            Lines::new("Bus disconnected.\n", Style::default().fg(Color::Red)),
        )
        .await;
}

#[allow(clippy::too_many_arguments)]
#[allow(clippy::await_holding_refcell_ref)]
async fn run_local(
    echo: Arc<AtomicBool>,
    client: SharedClient,
    triggers: Rc<RefCell<Matchers<Spans>>>,
    aliases: Rc<RefCell<Matchers<String>>>,
    sources: Arc<arc_swap::ArcSwap<rune::Sources>>,
    state_path: PathBuf,
    path: PathBuf,
    ui_rx: UnboundedReceiverStream<UiIn>,
    io_tx: IoSender,
    vars: rune::Value,
) {
    let mut input_rx = ui_rx.filter_map({
        let aliases = aliases.clone();
        let sources = sources.clone();
        let client = client.clone();
        move |event| {
            let state_path = state_path.clone();
            let path = path.clone();
            let io_tx = io_tx.clone();
            let client = client.clone();
            let triggers = triggers.clone();
            let aliases = aliases.clone();
            let sources = sources.clone();
            let vars = vars.clone();
            Box::pin(async move {
                match event {
                    UiIn::Command { id, command } => Some((id, command)),
                    UiIn::Reload { .. } => {
                        let (new_triggers, new_aliases, new_sources) =
                            match load(state_path, path, io_tx, client, vars).await {
                                Ok(v) => v,
                                Err(_) => return None,
                            };

                        tracing::debug!("swapping in new triggers/aliases");
                        *triggers.borrow_mut() = new_triggers;
                        *aliases.borrow_mut() = new_aliases;
                        sources.store(new_sources);

                        None
                    }
                }
            })
        }
    });
    loop {
        debug!("waiting for user input");
        let (id, input) = match input_rx.next().await {
            Some(c) => c,
            _ => {
                debug!("no more user input");
                return;
            }
        };

        if echo.load(Ordering::Relaxed) {
            let buffer_lines =
                Lines::new(format!("{input}\n"), Style::default().fg(Color::Magenta));
            debug!(?buffer_lines, "sending lines to buffer");
            let _ = client.append(&[(id, Mode::Primary)], buffer_lines).await;
        }

        // let cmds = split_cmds(&*input);
        let cmd = input;

        debug!(%cmd, "got input event");

        let handled: bool = match aliases.borrow().run(cmd.clone()).await {
            Ok(h) => h,
            Err(e) => {
                let mut writer = AnsiProcessor::new();
                e.emit(&mut writer, &sources.load())
                    .expect("unable to emit diagnostics");
                let _ = client.append(&[(id, Mode::Primary)], writer.take()).await;
                false
            }
        };
        if !handled {
            let mut cmd = cmd;
            debug!(?cmd, "sending command to remote");
            cmd.push_str("\r\n");
            if let Err(_e) = client.send(cmd).await {
                let _ = client
                    .append(
                        &[(id, Mode::Primary)],
                        Lines::new("Bus not connected.\n", Style::default().fg(Color::Red)),
                    )
                    .await;
            }
        }
    }
}
